module.exports = function(grunt) {

    // Load NPM Tasks
    require('load-grunt-tasks')(grunt);

    // Displays the elapsed execution time of grunt tasks
    require('time-grunt')(grunt);

    // 1. All configuration goes here 
    grunt.initConfig({

        // Store your Package file so you can reference its specific data whenever necessary
        pkg: grunt.file.readJSON('package.json'),

        // Configurable paths
        paths: {
            // Backend
            backend_theme:      'www/themes/backend',
            backend_sass:       'www/themes/backend/scss',
            backend_css:        'www/themes/backend/css',
            backend_js:         'www/themes/backend/js',

            // Frontend
            frontend_theme:     'www/themes/frontend',
            frontend_sass:      'www/themes/frontend/scss',
            frontend_css:       'www/themes/frontend/css',
            frontend_js:        'www/themes/frontend/_dev/js/**/*.js',
            frontend_tmp:       'www/themes/frontend/_dev/tmp',
        },

        // Run: `grunt watch` from command line for this section to take effect
        watch: {
            // frontend_css: {
            //     tasks: ['sass:frontend'],
            //     files: ['<%= paths.frontend_sass %>/**/*.scss']
            // },

            // frontend_js: {
            //     tasks: ['concat','uglify:dev'],
            //     files: ['<%= paths.frontend_js %>']
            // },

            backend_css: {
                tasks: ['sass:backend'],
                files: ['<%= paths.backend_sass %>/**/*.scss']
            },
        },

        // Javascript
        concat: {
            options: {
                //separator: ';',
            },
            dist: {
                src: [ '<%= paths.frontend_js %>' ],
                dest: '<%= paths.frontend_tmp %>/concat.js',
            },
            core_css: {
                src: [
                    '<%= paths.backend_theme %>/libraries/_remark/global/css/bootstrap.min.css',
                    '<%= paths.backend_theme %>/libraries/_remark/global/css/bootstrap-extend.min.css',
                    '<%= paths.backend_theme %>/libraries/_remark/assets/css/site.min.css',
                    '<%= paths.backend_theme %>/libraries/animsition/animsition.min.css',
                    '<%= paths.backend_theme %>/libraries/jquery-mmenu/jquery-mmenu.min.css',
                    '<%= paths.backend_theme %>/libraries/select2/select2.min.css',
                    '<%= paths.backend_theme %>/libraries/asscrollable/asScrollable.min.css',
                    '<%= paths.backend_theme %>/libraries/pnotify/jquery.pnotify.min.css',
                    '<%= paths.backend_theme %>/libraries/bootstrap-datepicker/bootstrap-datepicker.min.css',
                    '<%= paths.backend_theme %>/libraries/bootstrap-touchspin-4/jquery.bootstrap-touchspin.min.css',
                    '<%= paths.backend_theme %>/libraries/bootstrap-tokenfield/bootstrap-tokenfield.min.css',
                    '<%= paths.backend_theme %>/libraries/slidepanel/slidePanel.min.css',
                    '<%= paths.backend_theme %>/fonts/font-awesome/font-awesome.min.css',
                    '<%= paths.backend_theme %>/fonts/web-icons/dz-web-icons.min.css',
                    '<%= paths.backend_theme %>/libraries/bootstrap-colorpicker/css/bootstrap-colorpicker.min.css'
                ],
                dest: '<%= paths.backend_css %>/site.min.css'
            },
            core_js: {
                src: [
                    '<%= paths.backend_theme %>/libraries/babel-external-helpers/babel-external-helpers.js',
                    '<%= paths.backend_theme %>/libraries/tether/tether.min.js',
                    '<%= paths.backend_theme %>/libraries/bootstrap/bootstrap.min.js',
                    '<%= paths.backend_theme %>/libraries/animsition/animsition.min.js',
                    '<%= paths.backend_theme %>/libraries/mousewheel/jquery.mousewheel.min.js',
                    '<%= paths.backend_theme %>/libraries/asscrollbar/jquery-asScrollbar.min.js',
                    '<%= paths.backend_theme %>/libraries/asscrollable/jquery-asScrollable.min.js',
                    '<%= paths.backend_theme %>/libraries/jquery-mmenu/jquery.mmenu.min.all.js',
                    '<%= paths.backend_theme %>/libraries/select2/select2.full.min.js',
                    '<%= paths.backend_theme %>/libraries/scrollto/jquery.scrollTo.min.js',
                    '<%= paths.backend_theme %>/libraries/bootbox/jquery.bootbox.min.js',
                    '<%= paths.backend_theme %>/libraries/pnotify/jquery.pnotify.min.js',
                    '<%= paths.backend_theme %>/libraries/bootstrap-datepicker/bootstrap-datepicker.min.js',
                    '<%= paths.backend_theme %>/libraries/bootstrap-datepicker/bootstrap-datepicker.es.min.js',
                    '<%= paths.backend_theme %>/libraries/bootstrap-touchspin-4/jquery.number.min.js',
                    '<%= paths.backend_theme %>/libraries/bootstrap-touchspin-4/jquery.bootstrap-touchspin.min.js',
                    '<%= paths.backend_theme %>/libraries/bootstrap-tokenfield/bootstrap-tokenfield.min.js',
                    '<%= paths.backend_theme %>/libraries/matchheight/jquery.matchHeight-min.js',
                    '<%= paths.backend_theme %>/libraries/slidepanel/jquery-slidePanel.min.js',
                    '<%= paths.backend_theme %>/libraries/_remark/global/js/State.min.js',
                    '<%= paths.backend_theme %>/libraries/_remark/global/js/Component.min.js',
                    '<%= paths.backend_theme %>/libraries/_remark/global/js/Plugin.min.js',
                    '<%= paths.backend_theme %>/libraries/_remark/global/js/Base.min.js',
                    '<%= paths.backend_theme %>/libraries/_remark/global/js/Config.min.js',
                    '<%= paths.backend_theme %>/libraries/_remark/assets/js/Section/Menubar.min.js',
                    '<%= paths.backend_theme %>/libraries/_remark/assets/js/Section/Sidebar.min.js',
                    '<%= paths.backend_theme %>/libraries/_remark/assets/js/Section/PageAside.min.js',
                    '<%= paths.backend_theme %>/libraries/_remark/assets/js/Section/GridMenu.min.js',
                    '<%= paths.backend_theme %>/libraries/_remark/global/js/config/colors.min.js',
                    '<%= paths.backend_theme %>/libraries/_remark/assets/js/config/tour.min.js',
                    '<%= paths.backend_theme %>/libraries/_remark/assets/js/Site.min.js',
                    '<%= paths.backend_theme %>/libraries/_remark/global/js/Plugin/tabs.min.js',
                    '<%= paths.backend_theme %>/libraries/_remark/global/js/Plugin/asscrollable.min.js',
                    '<%= paths.backend_theme %>/libraries/_remark/global/js/Plugin/select2.min.js',
                    '<%= paths.backend_theme %>/libraries/_remark/global/js/Plugin/bootstrap-datepicker.min.js',
                    '<%= paths.backend_theme %>/libraries/_remark/global/js/Plugin/bootstrap-touchspin-4.js',
                    '<%= paths.backend_theme %>/libraries/bootstrap-colorpicker/js/bootstrap-colorpicker.js',
                    '<%= paths.backend_theme %>/js/dz.ajaxgrid.js',
                    '<%= paths.backend_theme %>/js/dz.gridview.js',
                    '<%= paths.backend_theme %>/js/dz.slidePanel.js',
                    '<%= paths.backend_theme %>/js/dz.slideTable.js',
                    '<%= paths.backend_theme %>/js/dz.fileStatusTable.js',
                    '<%= paths.backend_theme %>/js/scripts.js'
                ],
                dest: '<%= paths.backend_js %>/site.js'
            }
        },

        jshint: {
            options: {
                reporter: require('jshint-stylish'),
                curly:   true,
                eqeqeq:  true,
                immed:   true,
                latedef: true,
                newcap:  true,
                noarg:   true,
                sub:     true,
                undef:   true,
                boss:    true,
                eqnull:  true,
                browser: true,
                globals: {
                    jQuery: true
                },
            },
            dist: {
                src: ['Gruntfile.js', '<%= paths.frontend_js %>']
            }
            //afterconcat: ['<%= paths.frontend_tmp %>/concat.js']
        },

        uglify: {
            dev: {
                compress: {
                    ie8: false,
                    sequences: true,
                    //distperties: true,
                    dead_code: true,
                    drop_debugger: true,
                    comparisons: true,
                    conditionals: true,
                    evaluate: true,
                    booleans: true,
                    loops: true,
                    unused: true,
                    hoist_funs: true,
                    if_return: true,
                    join_vars: true,
                    cascade: true,
                    //negate_iife: true,
                    drop_console: false
                },
                files: {
                    '<%= paths.frontend_theme %>/js/main.min.js' : ['<%= paths.frontend_tmp %>/concat.js'],
                },
            },

            dist: {
                compress: {
                    ie8: false,
                    sequences: true,
                    //distperties: true,
                    dead_code: true,
                    drop_debugger: true,
                    comparisons: true,
                    conditionals: true,
                    evaluate: true,
                    booleans: true,
                    loops: true,
                    unused: true,
                    hoist_funs: true,
                    if_return: true,
                    join_vars: true,
                    cascade: true,
                    //negate_iife: true,
                    drop_console: true
                },

                files: {
                    '<%= paths.frontend_theme %>/js/main.min.js' : ['<%= paths.frontend_tmp %>/concat.js'],
                },
            },

            core_dist: {
                compress: {
                    ie8: false,
                    sequences: true,
                    //distperties: true,
                    dead_code: true,
                    drop_debugger: true,
                    comparisons: true,
                    conditionals: true,
                    evaluate: true,
                    booleans: true,
                    loops: true,
                    unused: true,
                    hoist_funs: true,
                    if_return: true,
                    join_vars: true,
                    cascade: true,
                    //negate_iife: true,
                    drop_console: true
                },

                files: {
                    '<%= paths.backend_js %>/site.min.js' : ['<%= paths.backend_js %>/site.js'],
                },
            },
        },

        // SASS
        sass: {
            backend: {
                options: {
                    style: 'compressed',
                    sourcemap: 'none'
                },
                files: {
                    '<%= paths.backend_css %>/modules/asset.css': '<%= paths.backend_sass %>/modules/asset.scss',
                    '<%= paths.backend_css %>/modules/category.css': '<%= paths.backend_sass %>/modules/category.scss',
                    '<%= paths.backend_css %>/modules/settings.css': '<%= paths.backend_sass %>/modules/settings.scss',
                    '<%= paths.backend_css %>/modules/user.css': '<%= paths.backend_sass %>/modules/user.scss',
                    '<%= paths.backend_css %>/modules/web.css': '<%= paths.backend_sass %>/modules/web.scss',
                    '<%= paths.backend_css %>/modules/commerce.css': '<%= paths.backend_sass %>/modules/commerce.scss',
                    '<%= paths.backend_css %>/style.min.css': '<%= paths.backend_sass %>/style.scss',
                }
            },

            frontend: {
                options: {
                    style: 'compressed',
                    sourcemap: 'none'
                },
                files: {
                    '<%= paths.frontend_css %>/main.min.css': '<%= paths.frontend_sass %>/main.scss',
                }
            }
        },
    });

    // 3. Where we tell Grunt what to do when we type "grunt" into the terminal
    // > grunt
    grunt.registerTask('default', [
        'watch',
    ]);

    // Create frontend files
    // > grunt frontend
    grunt.registerTask('frontend', [
        'sass:frontend', 'concat', 'uglify:dev'
    ]);

    // Watch backend files
    // > grunt backend
    grunt.registerTask('backend', [
        'watch:backend_css',
    ]);

    // Watch backend files
    // > grunt sass_backend
    grunt.registerTask('sass_backend', [
        'sass:backend',
    ]);

    // Unify all core CSS & Javascript files
    // > grunt core
    grunt.registerTask('core', [
        'concat:core_css',
        'concat:core_js',
        'uglify:core_dist'
    ]);
};

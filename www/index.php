<?php
/**
 * DZ web bootstrap file for webapp
 */

// Set path constants Root path
chdir(dirname(__FILE__).'/..');
define('DZ_BASE_PATH', dirname(__FILE__) .'/..');
define('DZ_APP_PATH', DZ_BASE_PATH .'/app');
define('DZ_STORAGE_PATH', DZ_BASE_PATH .'/storage');
define('DZ_CONFIG_PATH', DZ_BASE_PATH .'/app/config');
define('DZ_VENDOR_PATH', DZ_BASE_PATH .'/app/vendor');

// Include Composer autoload class
require DZ_VENDOR_PATH .'/autoload.php';

// Environment configuration 
if ( file_exists(DZ_BASE_PATH .'/.env') )
{
    (new Dotenv\Dotenv(DZ_BASE_PATH))->load();
}
// define('DZ_ENVIRONMENT', require DZ_BASE_PATH .'/app/config/env/which.php');
define('DZ_ENVIRONMENT', getenv('ENVIRONMENT') ?: 'prod');

// LIVE configuration
if ( DZ_ENVIRONMENT == 'prod' )
{
	defined('YII_DEBUG') or define('YII_DEBUG', false);
}

// DEV configuration
else
{
	defined('YII_DEBUG') or define('YII_DEBUG',true);
	defined('YII_TRACE_LEVEL') or define('YII_TRACE_LEVEL',3);

	// On dev display all errors
	error_reporting(-1);
	ini_set('display_errors', true);
}

// Include Yii Framework
// require DZ_VENDOR_PATH .'/yii/framework/yii.php';
require DZ_APP_PATH .'/core/Yii.php';
require DZ_APP_PATH .'/core/Dz.php';

// Include BOOTSTRAP configuration file
require DZ_CONFIG_PATH . '/bootstrap.php';

// Include configuration file
require DZ_APP_PATH . '/core/src/helpers/ConfigHelper.php';
$config = dz\helpers\ConfigHelper::merge([
    DZ_CONFIG_PATH . '/main.php',
    DZ_CONFIG_PATH . '/env/env.php',
    DZ_CONFIG_PATH . '/local/main-local.php'
]);

// Run Web Application
$app = Yii::createWebApplication($config);
$app->setControllerPath('app/core/src/controllers');
$app->run();

/* uncomment if you wish to debug your resulting config */
/* echo '<pre>' . dump($config) . '</pre>'; */

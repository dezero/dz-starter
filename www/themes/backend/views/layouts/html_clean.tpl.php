<?php
/*
|--------------------------------------------------------------------------
| Special HTML layout with clean content (just it)
|--------------------------------------------------------------------------
|
*/

use dz\helpers\StringHelper;

  /*
  |--------------------------------------------------------------------------
  | BODY CLASSES
  |--------------------------------------------------------------------------
  */
  $current_module = Yii::currentModule(true);
  $current_controller = Yii::currentController(true);
  $current_action = Yii::currentAction(true);

  $vec_current_params = [
    'current_module'      => $current_module,
    'current_controller'  => $current_controller,
    'current_action'      => $current_action,
  ];

  $vec_classes = [
    // Current action
    $current_action .'-page',
    
    // Current controller and action
    $current_controller .'-'. $current_action .'-page',
  
    // Logged in?
    ( Yii::app()->user->isGuest ? 'not-logged-in' : 'logged-in')
  ];
  
  $bodyClass = implode(" ", $vec_classes);
  
  // More body classes defined in controllers?
  if ( isset($this->bodyClass) )
  {
    $bodyClass .= $this->bodyClass;
  }
?>
<!DOCTYPE html>
<html lang="en">
  <?php
    // <head> & JSS & CSS files
    $this->renderPartial('//layouts/_html_head', $vec_current_params);
  ?>
  <body class="<?php echo $bodyClass; ?> clean-page">
    <?= $content; ?>
  </body>
</html>
<?php
/*
|--------------------------------------------------------------------------
| Main HTML layout
|--------------------------------------------------------------------------
*/

use dz\helpers\StringHelper;

/*
|--------------------------------------------------------------------------
| BODY CLASSES
|--------------------------------------------------------------------------
*/
  // Current params
  $vec_params = Yii::app()->backendManager->get_params();
  $current_action = Yii::app()->backendManager->current_action;
  $current_controller = Yii::app()->backendManager->current_controller;
  $current_module = Yii::app()->backendManager->current_module;

  // Get body classes
  $body_classes = implode(' ', Yii::app()->backendManager->body_classes());
?>
<!DOCTYPE html>
<html class="no-js css-menubar" lang="en">
<?php
  // <head> & JSS & CSS files
  $this->renderPartial('//layouts/_html_head', $vec_params);
?>
<body class="animsition <?= $body_classes; ?>">
<?php
  // Full layout - Login or change password page
  if ( Yii::app()->user->isGuest || ($current_module == 'user' && $current_controller == 'password') ) :
?>
  <div class="page vertical-align text-center">
    <div class="page-content vertical-align-middle <?= $current_controller .'-'. $current_action; ?>-content">
      <?= $content ?>
    </div>
  </div>
<?php
  // Registered users
  else :
?>
    <?php
      // HEADER
      $this->renderPartial('//layouts/_header', $vec_params);

      // SIDEBAR
      $this->renderPartial('//layouts/_sidebar', $vec_params);
    ?>
    <?php /*
      <!--main content start-->
      <section id="main-content"<?php if ( method_exists($this, 'is_full_width') && $this->is_full_width() ) : ?> class="merge-left"<?php endif; ?>>
          <section class="wrapper">
            <div class="sidebar-toogle-content-wrapper">
              <div id="sidebar-toggle-content-box" class="sidebar-toggle-box">
                <div class="fa fa-bars"></div>
              </div>
          </div>

          <?php
            //
            // |--------------------------------------------------------------------------
            // | FLASH MESSAGES
            // |--------------------------------------------------------------------------
            // 
            if ( !isset($_GET['opener']) ) :
          ?>
            <div id="flash-messages" class="flash-messages-wrapper">
              <div class="row">
                <div class="col-xs-12">
                  <?php
                    $this->widget('@bootstrap.widgets.TbAlert', array(
                      'block' => TRUE,    // display a larger alert block?
                      'fade' => TRUE,     // use transitions?
                      'closeText' => '×',   // close link text - if set to false, no close link is displayed
                    ));
                  ?>
                </div>
              </div>
            </div>
          <?php endif; ?>

            <!-- page start-->
            <?= $content; ?>
            <!-- page end-->
          </section>
      </section>
      <!--main content end-->
  </section>
  <section id="sidebar-left-fixed"></section>
  */ ?>
  <div class="page ">
    <?php
      //
      // |--------------------------------------------------------------------------
      // | FLASH MESSAGES
      // |--------------------------------------------------------------------------
      // 
      if ( !isset($_GET['opener']) ) :
    ?>
      <div id="flash-messages" class="flash-messages-wrapper container-fluid">
        <div class="row">
          <div class="col-lg-12">
            <?php
              $this->widget('@bootstrap.widgets.TbAlert', [
                'block' => true,    // display a larger alert block?
                'closeText' => '×',   // close link text - if set to false, no close link is displayed
              ]);
            ?>
          </div>
        </div>
      </div>
    <?php endif; ?>
    <?= $content ?>
  </div>
<?php endif; ?>
</body>
</html>

<?php
/*
|--------------------------------------------------------------------------
| Page content layout
|--------------------------------------------------------------------------
|
| Available variables:
|	$this: Controller
| 	$content: Contenido
|
*/
?>
<?php $this->beginContent('//layouts/html.tpl'); ?>
<?= $content; ?>
<?php $this->endContent(); ?>

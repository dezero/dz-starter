<?php
/*
|--------------------------------------------------------------------------
| HTML <head> layout
|--------------------------------------------------------------------------
*/

/*
|--------------------------------------------------------------------------
| CSS files
|--------------------------------------------------------------------------
*/
  // Unify CSS files?
  if ( Yii::app()->backendManager->is_unify_css )
  {
    Yii::app()->clientscript
      // CSS - Dz Framework CORE
      ->registerCssTopFile(Yii::app()->theme->baseUrl .'/css/site.min.css')

      // CSS - Custom
      ->registerCssLastFile(Yii::app()->theme->baseUrl .'/css/style.min.css');
  }

  // Separated CSS files?
  else
  {
    Yii::app()->clientscript
      // CSS - CORE
      ->registerCssTopFile(Yii::app()->theme->baseUrl .'/libraries/_remark/global/css/bootstrap.min.css')
      ->registerCssTopFile(Yii::app()->theme->baseUrl .'/libraries/_remark/global/css/bootstrap-extend.min.css')
      ->registerCssTopFile(Yii::app()->theme->baseUrl .'/libraries/_remark/assets/css/site.min.css')

      // CSS - Libraries / plugins
      ->registerCssTopFile(Yii::app()->theme->baseUrl .'/libraries/animsition/animsition.min.css')
      ->registerCssTopFile(Yii::app()->theme->baseUrl .'/libraries/jquery-mmenu/jquery-mmenu.min.css')
      ->registerCssTopFile(Yii::app()->theme->baseUrl .'/libraries/select2/select2.min.css')
      ->registerCssTopFile(Yii::app()->theme->baseUrl .'/libraries/asscrollable/asScrollable.min.css')
      ->registerCssTopFile(Yii::app()->theme->baseUrl .'/libraries/pnotify/jquery.pnotify.css')
      ->registerCssTopFile(Yii::app()->theme->baseUrl .'/libraries/bootstrap-datepicker/bootstrap-datepicker.min.css')
      ->registerCssTopFile(Yii::app()->theme->baseUrl .'/libraries/bootstrap-touchspin-4/jquery.bootstrap-touchspin.min.css')
      ->registerCssTopFile(Yii::app()->theme->baseUrl .'/libraries/bootstrap-tokenfield/bootstrap-tokenfield.min.css')

      // CSS - Fonts
      ->registerCssTopFile(Yii::app()->theme->baseUrl .'/fonts/web-icons/web-icons.min.css')

      // CSS - Custom stylesheets
      // ->registerCssLastFile(Yii::app()->theme->baseUrl .'/css/override.css')
      ->registerCssLastFile(Yii::app()->theme->baseUrl .'/css/style.min.css');
  }


/*
|--------------------------------------------------------------------------
| JAVSCRIPT files
|--------------------------------------------------------------------------
*/
  // Unify JS files?
  if ( Yii::app()->backendManager->is_unify_js )
  {
    Yii::app()->clientscript
      // Jquery
      ->registerScriptFile(Yii::app()->theme->baseUrl. '/libraries/jquery/jquery.min.js', CClientScript::POS_HEAD)

      // JS - Dz Framework CORE
      ->registerScriptFile(Yii::app()->theme->baseUrl. '/js/site.min.js', CClientScript::POS_END);
  }

  // Separated JS files?
  else
  {
    Yii::app()->clientscript
      // Javascript - CORE
      // ->registerCoreScript('jquery', CClientScript::POS_HEAD)
      ->registerScriptFile(Yii::app()->theme->baseUrl. '/libraries/jquery/jquery.min.js', CClientScript::POS_HEAD)
      ->registerScriptFile(Yii::app()->theme->baseUrl. '/libraries/babel-external-helpers/babel-external-helpers.js', CClientScript::POS_END)
      ->registerScriptFile(Yii::app()->theme->baseUrl. '/libraries/tether/tether.min.js', CClientScript::POS_END)
      ->registerScriptFile(Yii::app()->theme->baseUrl. '/libraries/bootstrap/bootstrap.min.js', CClientScript::POS_END)
      ->registerScriptFile(Yii::app()->theme->baseUrl. '/libraries/animsition/animsition.min.js', CClientScript::POS_END)
      ->registerScriptFile(Yii::app()->theme->baseUrl. '/libraries/mousewheel/jquery.mousewheel.min.js', CClientScript::POS_END)
      ->registerScriptFile(Yii::app()->theme->baseUrl. '/libraries/asscrollbar/jquery-asScrollbar.min.js', CClientScript::POS_END)
      ->registerScriptFile(Yii::app()->theme->baseUrl. '/libraries/asscrollable/jquery-asScrollable.min.js', CClientScript::POS_END)
      // ->registerScriptFile(Yii::app()->theme->baseUrl. '/libraries/ashoverscroll/jquery-asHoverScroll.min.js', CClientScript::POS_END)

      // Javascript - Libraries / plugins
      ->registerScriptFile(Yii::app()->theme->baseUrl. '/libraries/jquery-mmenu/jquery.mmenu.min.all.js', CClientScript::POS_END)
      ->registerScriptFile(Yii::app()->theme->baseUrl. '/libraries/select2/select2.full.min.js', CClientScript::POS_END)
      ->registerScriptFile(Yii::app()->theme->baseUrl. '/libraries/scrollto/jquery.scrollTo.min.js', CClientScript::POS_END)
      ->registerScriptFile(Yii::app()->theme->baseUrl. '/libraries/bootbox/jquery.bootbox.min.js', CClientScript::POS_END)
      ->registerScriptFile(Yii::app()->theme->baseUrl. '/libraries/pnotify/jquery.pnotify.min.js', CClientScript::POS_END)
      ->registerScriptFile(Yii::app()->theme->baseUrl. '/libraries/bootstrap-datepicker/bootstrap-datepicker.min.js', CClientScript::POS_END)
      ->registerScriptFile(Yii::app()->theme->baseUrl. '/libraries/bootstrap-datepicker/bootstrap-datepicker.es.min.js', CClientScript::POS_END)
      ->registerScriptFile(Yii::app()->theme->baseUrl. '/libraries/bootstrap-touchspin-4/jquery.number.min.js', CClientScript::POS_END)
      ->registerScriptFile(Yii::app()->theme->baseUrl. '/libraries/bootstrap-touchspin-4/jquery.bootstrap-touchspin.min.js', CClientScript::POS_END)
      ->registerScriptFile(Yii::app()->theme->baseUrl. '/libraries/bootstrap-tokenfield/bootstrap-tokenfield.min.js', CClientScript::POS_END)

      ->registerScriptFile(Yii::app()->theme->baseUrl. '/libraries/_remark/global/js/State.min.js', CClientScript::POS_END)
      ->registerScriptFile(Yii::app()->theme->baseUrl. '/libraries/_remark/global/js/Component.min.js', CClientScript::POS_END)
      ->registerScriptFile(Yii::app()->theme->baseUrl. '/libraries/_remark/global/js/Plugin.min.js', CClientScript::POS_END)
      ->registerScriptFile(Yii::app()->theme->baseUrl. '/libraries/_remark/global/js/Base.min.js', CClientScript::POS_END)
      ->registerScriptFile(Yii::app()->theme->baseUrl. '/libraries/_remark/global/js/Config.min.js', CClientScript::POS_END)
      ->registerScriptFile(Yii::app()->theme->baseUrl. '/libraries/_remark/assets/js/Section/Menubar.min.js', CClientScript::POS_END)
      ->registerScriptFile(Yii::app()->theme->baseUrl. '/libraries/_remark/assets/js/Section/Sidebar.min.js', CClientScript::POS_END)
      ->registerScriptFile(Yii::app()->theme->baseUrl. '/libraries/_remark/assets/js/Section/PageAside.min.js', CClientScript::POS_END)
      ->registerScriptFile(Yii::app()->theme->baseUrl. '/libraries/_remark/assets/js/Section/GridMenu.min.js', CClientScript::POS_END)

      ->registerScriptFile(Yii::app()->theme->baseUrl. '/libraries/_remark/global/js/config/colors.min.js', CClientScript::POS_END)
      ->registerScriptFile(Yii::app()->theme->baseUrl. '/libraries/_remark/assets/js/config/tour.min.js', CClientScript::POS_END)

      ->registerScriptFile(Yii::app()->theme->baseUrl. '/libraries/_remark/assets/js/Site.min.js', CClientScript::POS_END)
      ->registerScriptFile(Yii::app()->theme->baseUrl. '/libraries/_remark/global/js/Plugin/asscrollable.min.js', CClientScript::POS_END)
      ->registerScriptFile(Yii::app()->theme->baseUrl. '/libraries/_remark/global/js/Plugin/select2.min.js', CClientScript::POS_END)
      ->registerScriptFile(Yii::app()->theme->baseUrl. '/libraries/_remark/global/js/Plugin/bootstrap-datepicker.min.js', CClientScript::POS_END)
      ->registerScriptFile(Yii::app()->theme->baseUrl. '/libraries/_remark/global/js/Plugin/bootstrap-touchspin-4.js', CClientScript::POS_END)

      // Custom Javascript
      ->registerScriptFile(Yii::app()->theme->baseUrl. '/js/dz.ajaxgrid.js', CClientScript::POS_END)
      ->registerScriptFile(Yii::app()->theme->baseUrl. '/js/scripts.js', CClientScript::POS_END);
    }

  // Javascript variables
  Yii::app()->javascript->run();
?>
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=0, minimal-ui">
  <meta name="robots" content="noindex, nofollow">
  <meta name="googlebot" content="noindex">
  <meta name="description" content="<?= Yii::app()->name; ?> - Backend">
  <meta name="author" content="Dz Framework">
  <?php if (!Yii::app()->user->isGuest) : ?>
    <meta http-equiv="refresh" content="<?= Yii::app()->params['session_timeout'];?>;" />
  <?php endif; ?>

  <?php
    /**
     * FAVICON - Less is more
     * @see https://realfavicongenerator.net/blog/new-favicon-package-less-is-more/
     */
  ?>
  <?php /*
  <link rel="apple-touch-icon" sizes="180x180" href="<?= Yii::app()->request->baseUrl; ?>/images/favicon/apple-icon-180x180.png">
  <link rel="icon" type="image/png" sizes="32x32" href="<?= Yii::app()->request->baseUrl; ?>/images/favicon/favicon-32x32.png">
  <link rel="icon" type="image/png" sizes="16x16" href="<?= Yii::app()->request->baseUrl; ?>/images/favicon/favicon-16x16.png">
  <link rel="manifest" href="<?= Yii::app()->request->baseUrl; ?>/images/favicon/manifest.json">
  <link rel="mask-icon" href="/safari-pinned-tab.svg" color="#5bbad5">
  <meta name="msapplication-TileColor" content="#da532c">
  <meta name="msapplication-TileImage" content="<?= Yii::app()->request->baseUrl; ?>/images/favicon/ms-icon-144x144.png">
  */ ?>
  <meta name="theme-color" content="#ffffff">
  <?php /*
  <link rel="apple-touch-icon" href="<?= Yii::app()->theme->baseUrl; ?>/images/apple-touch-icon.png">
  <link rel="shortcut icon" href="<?= Yii::app()->theme->baseUrl; ?>/images/favicon.ico">
  */ ?>

  <?php if ( Yii::app()->user->isGuest ) : ?>
    <?php
      // Check if current action is login page (front page)
      if ( $current_action == 'login' ) :
    ?>
      <title><?= Yii::app()->name; ?></title>
    <?php else: ?>
      <title><?= $this->pageTitle; ?> | <?= Yii::app()->name; ?></title>
    <?php endif; ?>
  <?php else: ?>
    <title><?= $this->pageTitle; ?> | <?= Yii::app()->name; ?></title>
  <?php endif; ?>

  <!--[if lt IE 9]>
    <script src="<?= Yii::app()->theme->baseUrl; ?>/libraries/html5shiv/html5shiv.min.js"></script>
  <![endif]-->
  <!--[if lt IE 10]>
    <script src="<?= Yii::app()->theme->baseUrl; ?>/libraries/media-match/media.match.min.js"></script>
    <script src="<?= Yii::app()->theme->baseUrl; ?>/libraries/respond/respond.min.js"></script>
  <![endif]-->
  <script src="<?= Yii::app()->theme->baseUrl; ?>/libraries/breakpoints/breakpoints.min.js"></script>
  <script>Breakpoints();</script>
</head>
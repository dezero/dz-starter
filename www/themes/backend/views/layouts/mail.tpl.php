<?php
/*
|--------------------------------------------------------------------------
| Mail HTML layout
|--------------------------------------------------------------------------
|
| Based on HTML5 Email Boilerplate 0.5: http://htmlemailboilerplate.com/
|
| Available variables:
|	$this: Controller
| 	$content: Contenido
|
*/
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
	<meta name="viewport" content="width=device-width, initial-scale=1.0"/>
	<title><?= $data['subject']; ?></title>
	<style type="text/css">
		#outlook a {padding:0;}
		body{width:100% !important; -webkit-text-size-adjust:100%; -ms-text-size-adjust:100%; margin:0; padding:0;}
		.ExternalClass {width:100%;}
		.ExternalClass, .ExternalClass p, .ExternalClass span, .ExternalClass font, .ExternalClass td, .ExternalClass div {line-height: 100%;}
		#backgroundTable {margin:0; padding:0; width:100% !important; line-height: 100% !important;}
		img {outline:none; text-decoration:none; -ms-interpolation-mode: bicubic;}
		a img {border:none;}
		.image_fix {display:block;}
		p {margin: 1em 0;}
		h1, h2, h3, h4, h5, h6 {color: black !important;}
		h1 a, h2 a, h3 a, h4 a, h5 a, h6 a {color: blue !important;}
		h1 a:active, h2 a:active,  h3 a:active, h4 a:active, h5 a:active, h6 a:active {color: red !important;}
		h1 a:visited, h2 a:visited,  h3 a:visited, h4 a:visited, h5 a:visited, h6 a:visited {color: purple !important;}
		table td {border-collapse: collapse;}
		a {color: orange;}
		<?= $style; ?>
	</style>
	<?php
	/*
		<!--[if gte mso 9]>
			<style>
			// Target Outlook 2007 and 2010
			</style>
		<![endif]-->
	 */
	?>
</head>
<body>
<table cellpadding="0" cellspacing="0" border="0" id="backgroundTable">
	<tr>
        <td valign="top">
			<?= $content; ?>
			<?php
			/*
				<!-- Tables are the most common way to format your email consistently. Set your table widths inside cells and in most cases reset cellpadding, cellspacing, and border to zero. Use nested tables as a way to space effectively in your message. -->
				<table cellpadding="0" cellspacing="0" border="0" align="center">
					<tr>
						<td width="200" valign="top"></td>
						<td width="200" valign="top"></td>
						<td width="200" valign="top"></td>
					</tr>
				</table>
				<!-- End example table -->

				<!-- Yahoo Link color fix updated: Simply bring your link styling inline. -->
				<a href="http://htmlemailboilerplate.com" target ="_blank" title="Styling Links" style="color: orange; text-decoration: none;">Coloring Links appropriately</a>

				<!-- Gmail/Hotmail image display fix -->
				<img class="image_fix" src="full path to image" alt="Your alt text" title="Your title text" width="x" height="x" />

				<!-- Working with telephone numbers (including sms prompts).  Use the "mobile" class to style appropriately in desktop clients
				versus mobile clients. -->
				<span class="mobile_link">123-456-7890</span>
			 */
			?>
        </td>
	</tr>
</table>
</body>
</html>
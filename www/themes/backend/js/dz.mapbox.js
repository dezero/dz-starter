mapboxgl.accessToken = 'pk.eyJ1Ijoia2luZ3NvZm1hbWJvIiwiYSI6ImNrcGZvMHg4bDI5Yncyb254eWw0MGkzamIifQ.I2cUjyO82o_OcJXg71ia9A';

(function(document, window, $) {

  // Restaurant Map global object
  // -------------------------------------------------------------------------------------------
  $.dzMapbox = {
    // Properties
    container: 'map',
    $container: null,
    map: null,
    geocoder: null,
    marker: null,
    settings: {},

    // Preload marker images
    // @see https://codereview.stackexchange.com/questions/241556/best-way-to-preload-images-with-callback-on-completion-with-pure-javascript-es6?answertab=active#tab-top
    // ------------------------------------------------------------
    preloadImages: function() {
      var self = this;

      const preload = src => new Promise(function(resolve, reject) {
        const img = new Image();
        img.onload = function() {
          resolve(img);
        }
        img.onerror = reject;
        img.src = src;
      });

      const preloadAll = sources =>
        Promise.all(
          sources.map(
            preload));

      const sources = [];
      if ( self.settings.markers.images instanceof Array ) {
        self.settings.markers.images.forEach(function(image) {
          sources.push(self.getMarkerImageUrl(image.type));
        });
      }

      preloadAll(sources)
        // .then(images => console.log('Preloaded all', images))
        .catch(err => console.error('Failed', err));
    },

    // Return the full image URL for a marker
    // ------------------------------------------------------------
    getMarkerImageUrl(marker_type) {

      var self = this;
      var marker_image_url = '';

      if ( self.settings.markers.images instanceof Array ) {
        self.settings.markers.images.forEach(function(image) {
          if ( image.type === marker_type ) {
            marker_image_url = self.settings.markers.url + image.file;
          }
        });
      }

      return marker_image_url;
    },

    // Build drag & drop marker map
    // ------------------------------------------------------------
    buildMarkerMap: function(marker_type, options) {
      var self = this;

      // Default marker settings
      self.marker_settings = {
        $lat: null,
        $lot: null
      };

      if ( ! $.isEmptyObject(options) ) {
        $.extend(self.marker_settings, options);
      }

      // Create a div element for the marker
      const el = document.createElement('div');
      el.id                     = `marker-new`;
      el.className              = 'marker';
      el.style.backgroundImage  = 'url( '+ self.getMarkerImageUrl(marker_type) +' )';
      el.style.width            = '42px';
      el.style.height           = '50px';
      el.style.backgroundSize   = 'cover';

      // Create new Marker object
      self.marker = new mapboxgl.Marker( el, { draggable: true, offset: [0,-32] } )
          .setLngLat([self.settings.lon, self.settings.lat] )
          .addTo(self.map);

      // Event onDragEnd
      self.marker.on('dragend', function(e) {
        // New coordinates
        var new_coords = this.getLngLat();
        // console.log( 'Longitude: '+new_coords.lng+' / Latitude: '+new_coords.lat );

        // Update latitude and longitud form fields?
        if ( self.marker_settings.$lat.length > 0 ) {
          self.marker_settings.$lat.val(new_coords.lat);
        }
        if ( self.marker_settings.$lon.length > 0 ) {
          self.marker_settings.$lon.val(new_coords.lng);
        }

        // Reverse geocoding to get all address data
        self.reverseGeocoding(new_coords.lat, new_coords.lng);
      });
    },


    // Load Geocoder input search
    // ------------------------------------------------------------
    loadGeocoder: function() {
      var self = this;

      /**
       * Add the control to the map.
       * @see https://docs.mapbox.com/mapbox-gl-js/example/forward-geocode-custom-data/
       */
      self.geocoder = new MapboxGeocoder({
        accessToken: mapboxgl.accessToken,
        mapboxgl: mapboxgl,
        zoom: self.settings.zoom,
        placeholder: 'Busca',
        marker: false,
        // localGeocoder: function(query) {
        //   return $.dzMapbox.customGeocoder(query);
        // },
        render: function (item) {
          return $.dzMapbox.renderSearch(item);
        },

        // Limit search results number to 10
        limit: 7,

        // Limit seach results to Spain
        countries: 'es'
      });
    },

    // Render a search results
    // ------------------------------------------------------------
    renderSearch: function(item) {
      var placeName = item.place_name.split(',');
      /*
      if ( item.place_type === 'community' ) {
        //placeName[0] = '<img src="http://localhost/joinenergy/favicon-16x16.png" style="width: 16px;" class="inline-block pr-5"><span class="inline-block font-semibold">' + placeName[0] +'</span>';
        placeName[0] = '<img src="/favicon-16x16.png" style="width: 16px;" class="inline-block pr-5"><span class="inline-block font-semibold">' + placeName[0] +'</span>';
      }
      */
      return '<div class="mapboxgl-ctrl-geocoder--suggestion"><div class="mapboxgl-ctrl-geocoder--suggestion-title">' + placeName[0]+ '</div><div class="mapboxgl-ctrl-geocoder--suggestion-address">' + placeName.splice(1, placeName.length).join(',') + '</div></div>';
    },


    // Geoceder search
    // ------------------------------------------------------------
    addGeocoder: function() {
      var self = this;

      // Load Geocoder input search
      self.loadGeocoder();

      // Add geocoder outside the map
      // @see https://docs.mapbox.com/mapbox-gl-js/example/mapbox-gl-geocoder-outside-the-map/
      document.getElementById('geocoder').appendChild(self.geocoder.onAdd(self.map));

      // Event "result" fired when input is set
      // @see https://github.com/mapbox/mapbox-gl-geocoder/blob/master/API.md#on
      self.geocoder.on('result', (place) => {
        // Clear input search
        self.geocoder.clear();

        // Add new coordinates
        self.marker.setLngLat(place.result.center);

        // Fly to current location
        self.map.flyTo({
          center: place.result.center,
          zoom: self.settings.zoom
        });

        // Reverse geocoding to get all address data
        self.reverseGeocoding(place.result.center[1], place.result.center[0]);
      });
    },


    // Reverse geocoding via Mapbox API
    // ------------------------------------------------------------
    reverseGeocoding: function(lat, lon) {
      $.ajax({
        url: js_globals.base_url +"/search/mapbox/reverseGeocode",
        type: 'POST',
        cache: false,
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        data: JSON.stringify({
          latitude: lat,
          longitude: lon,
          access_token: mapboxgl.accessToken
        }),
        success: function(data) {
          $.dzMapbox.afterReverseGeocoding(data);
        },
        error: function(xhr, ajaxOptions, thrownError) {
          console.log('Unable to access to Mapbox reverse geocoding');
        }
      });
    },

    // Custom event "afterReverseGeocoding"
    // ------------------------------------------------------------
    afterReverseGeocoding: function(data) {
    },

    // Load map
    // ------------------------------------------------------------
    load: function(container, options){
      var self = this;

      // Container
      self.container = container;
      self.$container = $('#'+ container);

      // Default settings
      self.settings = {
        lat: 2.1894214,
        lon: 41.3959589,
        zoom: 16,

        // Marker image URL from data
        markers: {
          url: null,
          images: []
        }
      }

      if ( ! $.isEmptyObject(options) ) {
        $.extend(self.settings, options);
      }

      // Map properties from data attributes
      self.settings.lat = self.$container.is('[data-lat]') ? self.$container.data('lat') : self.settings.lat;
      self.settings.lon = self.$container.is('[data-lon]') ? self.$container.data('lon') : self.settings.lon;
      self.settings.zoom = self.$container.is('[data-zoom]') ? self.$container.data('zoom') : self.settings.zoom;

      // Marker image URL from data
      self.settings.markers.url = self.$container.is('[data-markers-url]') ? self.$container.data('markers-url') : self.settings.markers.url;

      // Property "Map"
      // -------------------------------
      self.map = new mapboxgl.Map({
        container: self.container,
        style: 'mapbox://styles/mapbox/light-v10',
        center: [self.settings.lon, self.settings.lat],
        zoom: self.settings.zoom,
        scrollZoom: true,
        maxZoom: 18,
        minZoom: 5,
        maxBounds: [
          [-11.519051, 35.644605],    // Spain - Southwest coordinates
          [4.892373, 44.097952]       // Spain - Northeast coordinates
        ]
      }),

      // Preload images
      self.preloadImages();

      // Add zoom and rotation controls to the map
      self.map.addControl(new mapboxgl.NavigationControl());

      // Add geocoder search
      self.addGeocoder();
    },
  };

})(document, window, jQuery);

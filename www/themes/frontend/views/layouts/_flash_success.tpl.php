<?php
/*
|--------------------------------------------------------------------------
| Flash SUCCESS message partial
|--------------------------------------------------------------------------
*/
?>
<div class="messages-success alert alert-success" role="alert">
  <?php echo Yii::app()->user->getFlash('success'); ?>
</div>
<?php
/*
|--------------------------------------------------------------------------
| Main HTML layout
|--------------------------------------------------------------------------
*/

  use dz\helpers\StringHelper;
  use dz\helpers\Url;

/*
|--------------------------------------------------------------------------
| BODY CLASSES
|--------------------------------------------------------------------------
*/
  // Current params
  $vec_params = Yii::app()->frontendManager->get_params();
  $current_action = Yii::app()->frontendManager->current_action;
  $current_controller = Yii::app()->frontendManager->current_controller;
  $current_module = Yii::app()->frontendManager->current_module;
  $language_id = Yii::app()->language;

  // Get body classes
  $body_classes = implode(' ', Yii::app()->frontendManager->body_classes());

  // Disable CORE javascript files
  Yii::app()->frontendManager->disable_core_js();
?>
<!DOCTYPE HTML>
<html class="no-js" lang="<?= $language_id; ?>">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="x-ua-compatible" content="ie=edge">
    <meta name="robots" content="noindex, nofollow">
    <meta name="googlebot" content="noindex">
    <title><?= Yii::app()->frontendManager->page_title(); ?></title>
    <meta name="description" content="">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <?php
      // LANGUAGE TAGS
    ?>
    <link rel="canonical" href="<?= Url::canonical(); ?>" />
    <link rel="alternate" hreflang="es" href="<?= Url::current_slug('es'); ?>" />
    <link rel="alternate" hreflang="en" href="<?= Url::current_slug('en'); ?>" />

    <!-- Place favicon.ico in the root directory -->

    <?php
    /*
    <link href="https://fonts.googleapis.com/css?family=Roboto:100i,300,300i,400,400i,500,500i,700,700i,900,900i&display=swap" rel="stylesheet">
    <link href="https://fonts.googleapis.com/css?family=Poppins:300,400,500,600,700&display=swap" rel="stylesheet">
    <link rel="stylesheet" href="<?= Url::theme(); ?>/assets/css/libs/bootstrap.min.css">
    <link rel="stylesheet" href="<?= Url::theme(); ?>/assets/css/libs/bootstrap-datepicker.min.css">
    <link rel="stylesheet" href="<?= Url::theme(); ?>/assets/css/libs/bootstrap-datepicker.standalone.min.css">
    <link rel="stylesheet" href="<?= Url::theme(); ?>/assets/css/main.min.css">
    */
    ?>
  </head>
  <body<?php if ( !empty($body_class) ) : ?> class="<?= StringHelper::trim($body_class); ?>"<?php endif; ?>>
    <?php
      // HEADER
      // $this->renderPartial('//layouts/_header', $vec_params);
    ?>
    <?php
      // SUCCESS ALERTS (FLASH MESSAGES)
      if ( Yii::app()->user->hasFlash('success') ) :
    ?>
      <?= $this->renderPartial('//layouts/_flash_success'); ?>
    <?php endif; ?>
    <?php
      // ERROR ALERTS (FLASH MESSAGES)
      if ( Yii::app()->user->hasFlash('error') ) :
    ?>
      <?= $this->renderPartial('//layouts/_flash_error'); ?>
    <?php endif; ?>
    <?php
      // CONTENT
      echo $content;

      // FOOTER
      // $this->renderPartial('//layouts/_footer', $vec_params);
    ?>
    <?php
      // Javascript GLOBAL variables
      Yii::app()->javascript->run();
    ?>
    <script src="<?= Url::theme(); ?>/assets/js/main.min.js"></script>
  </body>
</html>
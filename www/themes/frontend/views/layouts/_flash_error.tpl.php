<?php
/*
|--------------------------------------------------------------------------
| Flash SUCCESS message partial
|--------------------------------------------------------------------------
*/
?>
<div class="messages-error alert alert-danger alert-dismissible fade show" role="alert">
  <?php echo Yii::app()->user->getFlash('error'); ?>
</div>
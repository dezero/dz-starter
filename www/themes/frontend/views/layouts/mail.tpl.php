<?php
/*
|--------------------------------------------------------------------------
| Mail HTML layout
|--------------------------------------------------------------------------
|
| Based on HTML5 Email Boilerplate 0.5: http://htmlemailboilerplate.com/
|
| Available variables:
|  - $this: Controller
|  - $data: Content
|
*/
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
  <head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <!--[if !mso]><!-->
        <meta http-equiv="X-UA-Compatible" content="IE=edge" />
    <!--<![endif]-->
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title><?= $data['subject']; ?></title>

    <style type="text/css">


      body { Margin: 0; padding: 0; min-width: 100%; background-color: #ffffff; }
      table { border-spacing: 0; font-family: sans-serif; color: #333333; }
      td { padding: 0; }
      img { border: 0; height: auto; width: auto; max-width: 800px; }
      .wrapper { width: 100%; table-layout: fixed; -webkit-text-size-adjust: 100%; -ms-text-size-adjust: 100%; <?php /*border-top: 2px solid #441C75;*/ ?> }
      .webkit { max-width: 800px; }

      .outer { Margin: 0 auto; width: 100%; max-width: 800px; }

      .inner { padding: 10px; }
      .contents { width: 100%; }
      p { Margin: 0; font-size: 15px; line-height: 22px; Margin-bottom: 22px; }
      td p { Margin-bottom: 5px; }
      a { color: #9EC700; text-decoration: underline; }
      .h1 { font-size: 36px; font-weight: bold; Margin-bottom: 10px; color:#fff;}
      .h2 {padding: 0 0 15px 0; font-size: 18px; line-height: 28px; font-weight: bold; Margin-bottom: 12px; color: #fff; }

      .full-width-image img { width: 100%; max-width: 800px; height: auto; }
      .label { font-weight: bold; }
      ul li {  font-size: 13px; line-height: 18px; margin-bottom: 15px; } 

      <?= $style; ?>
    </style>
    <!--[if (gte mso 9)|(IE)]>
    <style type="text/css">
        table {border-collapse: collapse !important;}
    </style>
    <![endif]-->
  </head>
  <body style="Margin:0;padding-top:0;padding-bottom:0;padding-right:0;padding-left:0;min-width:100%;background-color:#ffffff;" >
    <div class="wrapper" style="width:100%;table-layout:fixed;-webkit-text-size-adjust:100%;-ms-text-size-adjust:100%;<?php /*border-top-width:2px;border-top-style:solid;border-top-color:#441C75;*/ ?>" >
      <div class="webkit" style="max-width:800px;" >
        <br>
        <?= $content; ?>
      </div>
    </div>
  </body>
</html>
<?php
/*
|--------------------------------------------------------------------------
| Error page
|--------------------------------------------------------------------------
|
| Available variables:
|  - $code: Error code
|  - $message: Error message
|
*/
  use dz\helpers\Html;
  use dz\helpers\Url;

  $this->pageTitle .= ' '. $code;
?>
<section class="error-section"> 
  <div class="error-container error-wrapper text-center">
    <h1>ERROR <span class="light-text"><?= Html::encode($code); ?></span></h1>
    
    <?php if ( $code == 404 ) : ?>
      <h2><?= Yii::t('frontend', 'Page not found'); ?></h2>
    <?php else : ?>
      <h2><?= Html::encode(Yii::t('app', $message)); ?></h2>
    <?php endif; ?>
    
    <div class="error-actions">
      <br><br>
      <p>
        <a class="btn btn-primary" href="<?= Url::home(); ?>"><?= Yii::t('app', 'GO TO HOME PAGE'); ?></a>
      </p>
    </div>
  </div>
</section>
<?php
/**
 * console.php
 *
 * This file holds the configuration settings for yiic console application
 * Any writable CConsoleApplication properties can be configured here.
 **/

// Core path
// Yii::setAlias('@core', $root. DIRECTORY_SEPARATOR . 'app' . DIRECTORY_SEPARATOR . 'core');

// Params local file (not included on GIT repository)
$vec_params = require DZ_CONFIG_PATH . DIRECTORY_SEPARATOR . 'local'. DIRECTORY_SEPARATOR .'params.php';

return [
    'name' => getenv('SITE_NAME'),

    // Custom paths
    'basePath' => dirname(__FILE__) . DIRECTORY_SEPARATOR . '..',
    'runtimePath' => DZ_BASE_PATH . DIRECTORY_SEPARATOR . 'storage'. DIRECTORY_SEPARATOR .'runtime',
    'commandPath' => Yii::getAlias('@core') . DIRECTORY_SEPARATOR .'src'. DIRECTORY_SEPARATOR .'commands', 

    // preloading 'log' component
    'preload' => ['log'],

    // Aliases
    'aliases' => require DZ_CONFIG_PATH . DIRECTORY_SEPARATOR . 'common'. DIRECTORY_SEPARATOR .'aliases.php',

    // setup import paths aliases
    // @see http://www.yiiframework.com/doc/api/1.1/YiiBase#import-detail
    // autoloading model and component classes
    'import' => [
        // Glogal DzLog class
        '@core.src.helpers.DzLog',
        
        // '@core.models.*',
        // '@core.components.*',
        // '@core.helpers.*',

        // Yii-user module --> http://www.yiiframework.com/extension/yii-user/
        '@modules.user.models.*',
        '@modules.user.components.*',
    ],
        
    // application components
    'components' => require DZ_CONFIG_PATH . DIRECTORY_SEPARATOR .'common'. DIRECTORY_SEPARATOR . 'components.php',

    // Yii modules
    'modules' => require DZ_CONFIG_PATH . DIRECTORY_SEPARATOR .'common'. DIRECTORY_SEPARATOR . 'modules.php',

    // Locale settings
    'language' => 'es',         // <--- COPY THIS VALUE TO "default_language" property on /app/config/components/i18n.php
    'sourceLanguage' => '00',
    'timeZone' => 'Europe/Madrid',

    // Migrations
    'commandMap'=> dz\helpers\ConfigHelper::merge([
        [
            'migrate'=> [
                // 'class'          => '@core.components.DzMigrateCommand.DzMigrateCommand',
                'class'             => 'dz\console\MigrateCommand',
                'migrationPath'     => '@core.src.migrations',
                'migrationTable'    => 'migration',
                'templateFile'      => '@core.src.console.views.migration',
            ],
            'cc' => [
                'class' => '\dz\commands\CcCommand',
            ],
            'ext' => [
                'class' => '\dz\commands\ExtCommand',
            ],
            'search' => [
                'class' => '\dz\commands\SearchCommand',
            ],
        ],
        DZ_BASE_PATH . '/app/config/components/commands.php'
    ]),

    // Application-level parameters that can be accessed
    // using Yii::app()->params['paramName']
    'params'=> $vec_params,
];
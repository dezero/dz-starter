<?php
/**
 * Categories configuration
 * 
 * Load it via Yii::app()->config->get('components.categories')
 */
return [
	// <CATEGORY_TYPE> "category" is the default category type
	'category' => [
        // Actions allowed
        'is_editable'               => true,
        'is_disable_allowed'        => true,
        'is_delete_allowed'         => true,
        'is_first_level_sortable'   => true,
        
        // Max levels (subcategories)
        'max_levels'                => 3,

        // Optional fields
        'is_multilanguage'          => true,
        'is_description'            => true,
        'is_image'                  => true,
        'is_seo_fields'             => true,

        // Custom path for images
        'images_path'               => 'www' . DIRECTORY_SEPARATOR .'files'. DIRECTORY_SEPARATOR .'images'. DIRECTORY_SEPARATOR .'category'. DIRECTORY_SEPARATOR,

        // View files path
        'views' => [
            'index'         => '//category/_base/index',
            'create'        => '//category/_base/create',    
            'update'        => '//category/_base/update',
            '_form'         => '//category/_base/_form',
            '_form_seo'     => '//category/_base/_form_seo',
            '_grid_column'  => '//category/_base/_grid_column',
            '_tree'         => '//category/_base/_tree',
            '_tree_main'    => '//category/_base/_tree_main',
        ],

        // Texts
        'texts' => [
            'entity_label'      => 'Category',
            'subentity_label'   => 'Subcategory',
            'entities_label'    => 'Categories',

            'index_title'       => 'Manage categories',
            'panel_title'       => 'Categories',
            'list_title'        => 'Categories list',
            'add_button'        => 'Add category',
            'create_title'      => 'Create category',
            'subcategory_title' => 'Create subcategory of "{subcategory}"',

            // Success messages
            'created_success'   => 'New category created successfully',
            'updated_success'   => 'Category updated successfully',

            // Disable
            'disable_success'   => 'Category DISABLED successfully',
            'disable_error'     => 'Category could not be DISABLED',
            'disable_confirm'   => '<h3>Are you sure you want to <span class=\'text-danger\'>DISABLE</span> this category?</h3>',

            // Enable
            'enable_success'    => 'Category ENABLED successfully',
            'enable_error'      => 'Category could not be ENABLED',
            'enable_confirm'    => '<h3>Are you sure you want to <span class=\'text-success\'>ENABLE</span> this category?</h3>',

            // Delete
            'delete_success'    => 'Category DELETED successfully',
            'delete_error'      => 'Category could not be DELETED',
            'delete_confirm'    => '<h3><i class="icon wb-alert-circle text-danger"></i> Are you sure you want to <span class=\'text-danger\'>DELETE</span> this category?</h3>',

            // Other
            'subcategories'     => 'subcategories',
            'empty_text'        => 'No categories found',
        ]
	],

    // Category type "TAG"
    'tag' => [
        // Actions allowed
        'is_editable'               => true,
        'is_disable_allowed'        => true,
        'is_delete_allowed'         => true,
        'is_first_level_sortable'   => false,
        
        // Max levels (subcategories)
        'max_levels'                => 1,

        // Optional fields
        'is_multilanguage'          => false,
        'is_description'            => false,
        'is_image'                  => false,
        'is_seo_fields'             => false,

        // View files path
        'views' => [
            'index'         => '//category/_base/index',
            'create'        => '//category/_base/create',    
            'update'        => '//category/_base/update',
            '_form'         => '//category/_base/_form',
            '_form_seo'     => '//category/_base/_form_seo',
            '_grid_column'  => '//category/_base/_grid_column',
            '_tree'         => '//category/_base/_tree',
            '_tree_main'    => '//category/_base/_tree_main',
        ],

        // Texts
        'texts' => [
            'entity_label'      => 'Tag',
            'subentity_label'   => 'Subtag',
            'entities_label'    => 'Tags',

            'index_title'       => 'Manage tags',
            'panel_title'       => 'Tags',
            'list_title'        => 'Tags list',
            'add_button'        => 'Add tag',
            'create_title'      => 'Create tag',
            'subcategory_title' => 'Create subtag of "{subcategory}"',

            // Success messages
            'created_success'   => 'New tag created successfully',
            'updated_success'   => 'Tag updated successfully',

            // Disable
            'disable_success'   => 'Tag DISABLED successfully',
            'disable_error'     => 'Tag could not be DISABLED',
            'disable_confirm'   => '<h3>Are you sure you want to <span class=\'text-danger\'>DISABLE</span> this tag?</h3>',

            // Enable
            'enable_success'    => 'Tag ENABLED successfully',
            'enable_error'      => 'Tag could not be ENABLED',
            'enable_confirm'    => '<h3>Are you sure you want to <span class=\'text-success\'>ENABLE</span> this tag?</h3>',

            // Delete
            'delete_success'    => 'Tag DELETED successfully',
            'delete_error'      => 'Tag could not be DELETED',
            'delete_confirm'    => '<h3><i class="icon wb-alert-circle text-danger"></i> Are you sure you want to <span class=\'text-danger\'>DELETE</span> this tag?</h3>',

            // Other
            'subcategories'     => 'subtags',
            'empty_text'        => 'No tags found',
        ]
    ],
];
<?php
/**
 * Log files
 */
 return [
    [
        'class'=>'\dz\log\FileLogRoute',
        'logFile' => 'app.log',
        'levels'=>'error, warning',
        'filter' => [
            'class' => 'CLogFilter',
            'logUser' => true,      // Add user information into LOG
            'logVars' => []         // Remove globals variables: $_GET, $_POST, $_SERVER, ...
        ]
    ],
    [
        'class' => '\dz\log\FileLogRoute',
        'logFile' => 'app.error.log',
        'categories' => 'error',
    ],
    [
        'class' => '\dz\log\FileLogRoute',
        'logFile' => 'app.warning.log',
        'categories' => 'warning',
    ],
    [
        'class' => '\dz\log\FileLogRoute',
        'logFile' => 'mail.error.log',
        'categories' => 'mail_error',
    ],
    [
        'class' => '\dz\log\FileLogRoute',
        'logFile' => 'mail.cron.log',
        'categories' => 'mail_cron',
    ],
    [
        'class' => '\dz\log\FileLogRoute',
        'logFile' => 'mailer.log',
        'categories' => 'mailer',
    ],
    [
        'class' => '\dz\log\FileLogRoute',
        'logFile' => 'dev.log',
        'categories' => 'dev',
    ],
    [
        'class' => '\dz\log\FileLogRoute',
        'logFile' => 'cron.log',
        'categories' => 'cron',
    ],
    [
        'class' => '\dz\log\FileLogRoute',
        'logFile' => 'files.log',
        'categories' => 'ext.file',
        'levels' => 'error, info'
    ],
    [
        'class' => '\dz\log\FileLogRoute',
        'logFile' => 'images.log',
        'categories' => 'images',
    ],
    [
        'class' => '\dz\log\FileLogRoute',
        'logFile' => 'denied_location.log',
        'categories' => 'denied_location',
    ],
    [
        'class' => '\dz\log\FileLogRoute',
        'logFile' => 'login.access.log',
        'categories' => 'login_access',
    ],
    [
        'class' => '\dz\log\FileLogRoute',
        'logFile' => 'login.error.log',
        'categories' => 'login_error',
    ],
    [
        'class' => '\dz\log\FileLogRoute',
        'logFile' => 'download.info.log',
        'categories' => 'download_info',
    ],
    [
        'class' => '\dz\log\FileLogRoute',
        'logFile' => 'download.error.log',
        'categories' => 'download_error',
    ],

    // Logs for API REST requests
    [
        'class' => '\dz\log\FileLogRoute',
        'logFile' => 'rest.log',
        'categories' => 'rest',
    ],
    [
        'class' => '\dz\log\FileLogRoute',
        'logFile' => 'rest.error.log',
        'categories' => 'rest_error',
    ],

    // Log for sitemap generation & other seo task
    [
        'class' => '\dz\log\FileLogRoute',
        'logFile' => 'seo.log',
        'categories' => 'seo',
    ],

    /*
    // Database debug
    [
        'class'=>'\dz\log\FileLogRoute',
        'levels'=>'trace,log',
        'categories' => 'system.db.CDbCommand',
        'logFile' => 'db.log',
    ],
    */
    /*
    // uncomment the following to show log messages on web pages
    [
        'class'=>'CWebLogRoute',
    ],
    */
    /*
    // Yii-debug-toolbar extension - http://www.yiiframework.com/extension/yii-debug-toolbar/
    [
        'class' => '@lib.yii-debug-toolbar.YiiDebugToolbarRoute',
        'ipFilters'=> ['127.0.0.1', '::1', '192.168.1.111'],
        'enabled' => FALSE, // YII_DEBUG
    ],
    */
];
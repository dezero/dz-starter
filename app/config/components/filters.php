<?php
/**
 * Controller filter configurations
 * 
 * Load it via Yii::app()->config->get('components.filters')
 */
return [
  // Default core auth filter
  ['dz\filters\AuthFilter'],

  // Access by IP address
  /*
  [
    'dz\filters\IpLocationFilter',
    
    // IP addresses excluded (usually, localhost)
    'excluded_ip_addresses' => ['127.0.0.1', '::1'],

    // Allow access only for selected IP addresses
    'allowed_ip_addresses' => [],

    // Allow access only for selected countries
    'allowed_countries' => ['ES']
  ],
  */
];
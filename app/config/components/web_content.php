<?php
/**
 * Web content page configuration
 * 
 * Load it via Yii::app()->config->get('components.web_content')
 */
return [
    // Type "slideshow"
    'slideshow' => [
        'is_editable' => true,
        'is_multilanguage' => false,
        'is_seo_fields' => false,
        'is_sortable' => true,
        'is_disable_allowed' => true,
        'is_delete_allowed' => true,
        
        // Attributes
        'attributes' => [
            'title' => [
                'is_translatable'   => false
            ],
            'subtitle' => [
                'is_translatable'   => false
            ],
            'image_id' => [
                'label' => 'Imagen',
                'images_path' => Yii::app()->path->imagesPath() . DIRECTORY_SEPARATOR . 'slideshow' . DIRECTORY_SEPARATOR,
            ],
            'link_url' => [
                'label' => 'Enlace',
                'is_translatable'   => false
            ],
        ],

        // View files path
        'views' => [
            'index'         => '//web/_base/index',
            'create'        => '//web/_base/create',    
            'update'        => '//web/_base/update',
            '_grid_column'  => '//web/_base/_grid_column',
            '_form'         => '//web/_base/_form',
            '_form_seo'     => '//web/_base/_form_seo',
            '_tree'         => '//web/_base/_tree',
        ],

        // Texts
        'texts' => [
            'entity_label'      => 'Slideshow',
            'entities_label'    => 'Slideshow',

            'index_title'       => 'Manage slideshows',
            'add_button'        => 'Add slideshow',
            'create_title'      => 'Create slideshow',
            'panel_title'       => 'Sort slideshows',
            'list_title'        => 'Slideshow list',

            // Success messages
            'created_success'   => 'New slideshow created successfully',
            'updated_success'   => 'Slideshow updated successfully',

            // Disable
            'disable_success'   => 'Slideshow DISABLED successfully',
            'disable_error'     => 'Slideshow could not be DISABLED',
            'disable_confirm'   => '<h3>Are you sure you want to <span class=\'text-danger\'>DISABLE</span> this slideshow?</h3><p><u class=\'text-danger\'>WARNING:</u> This slideshow will not be visible.</p>',

            // Enable
            'enable_success'    => 'Slideshow ENABLED successfully',
            'enable_error'      => 'Slideshow could not be ENABLED',
            'enable_confirm'    => '<h3>Are you sure you want to <span class=\'text-success\'>ENABLE</span> this slideshow?</h3><p>The slideshow is going to be visible.</p>',

            // Delete
            'delete_success'    => 'Slideshow DELETED successfully',
            'delete_error'      => 'Slideshow could not be DELETED',
            'delete_confirm'    => '<h3><i class="icon wb-alert-circle text-danger"></i> Are you sure you want to <span class=\'text-danger\'>DELETE</span> this slideshow?</h3><p><strong>WARNING:</strong> The slideshow will be permanently removed. Consider disabling it.</p>',

            // Other
            'empty_text'        => 'No slideshows found',
        ]
    ],

    // Type "document"
    'document' => [
        'is_editable' => true,
        'is_multilanguage' => false,
        'is_seo_fields' => false,
        'is_sortable' => true,
        'is_disable_allowed' => true,
        'is_delete_allowed' => true,
        
        // Attributes
        'attributes' => [
            'title' => [
                'label' => 'Título Documento',
                'is_translatable'   => false
            ],
             'subtitle' => [
                'label' => Yii::t('backend', 'Tipo'),
                'is_translatable'   => true,
                'allowed_values' => ['Tipo 1', 'Tipo 2', 'Tipo 3']
            ],
            'body' => [
                'label'             => 'Descripción',
                'is_translatable'   => false,
                'min_height'        => 150,
                'plugin'            => 'markdown'   // 'redactor'
            ],
            'file_id' => [
                'label' => 'Fichero',
                'files_path' => Yii::app()->path->filesPath() . DIRECTORY_SEPARATOR . 'document' . DIRECTORY_SEPARATOR,
            ],
        ],

        // View files path
        'views' => [
            'index'         => '//web/_base/index',
            'create'        => '//web/_base/create',    
            'update'        => '//web/_base/update',
            '_grid_column'  => '//web/_base/_grid_column',
            '_form'         => '//web/_base/_form',
            '_form_seo'     => '//web/_base/_form_seo',
            '_tree'         => '//web/_base/_tree',
        ],

        // Texts
        'texts' => [
            'entity_label'      => 'Documento',
            'entities_label'    => 'Documento',

            'index_title'       => 'Gestionar documentos',
            'add_button'        => 'Añadir documento',
            'create_title'      => 'Crear documento',
            'panel_title'       => 'Ordernar documentos',
            'list_title'        => 'Documentos',

            // Success messages
            'created_success'   => 'Nuevo documento creado correctamente',
            'updated_success'   => 'Documento actualizado correctamente',

            // Disable
            'disable_success'   => 'Documento DADO DE BAJA correctamente',
            'disable_error'     => 'No se puede DAR DE BAJA el documento',
            'disable_confirm'   => '<h3>¿Estás seguro de <span class=\'text-danger\'>DAR DE BAJA</span> este documento?</h3><p><u class=\'text-danger\'>AVISO:</u> Este documento no estará visible.</p>',

            // Enable
            'enable_success'    => 'Documento DADO DE ALTA correctamente',
            'enable_error'      => 'No se puede DAR DE ALTA el documento',
            'enable_confirm'   => '<h3>¿Estás seguro de <span class=\'text-success\'>DAR DE ALTA</span> este documento?</h3><p><u class=\'text-danger\'>AVISO:</u> Este documento volverá a estar visible.</p>',

            // Delete
            'delete_success'    => 'Documento ELIMINADO correctamente',
            'delete_error'      => 'No se puede ELIMINAR el documento',
            'delete_confirm'    => '<h3><i class="icon wb-alert-circle text-danger"></i> ¿Estás seguro de que quieres <span class=\'text-danger\'>ELIMINAR</span> este documento?</h3><p><strong>AVISO:</strong> El documento será eliminado de FORMA PERMANENTE.</p>',

            // Other
            'empty_text'        => 'No se han encontrado documentos'
        ]
    ],

     // Type "page"
    'page' => [
        'is_editable' => true,
        'is_multilanguage' => false,
        'is_seo_fields' => true,
        'is_sortable' => false,
        'is_disable_allowed' => true,
        'is_delete_allowed' => true,
        
        // Attributes
        'attributes' => [
            'title' => [
                'is_translatable'   => true
            ],
            'body' => [
                'is_translatable'   => true,
                'min_height'        => 350,
                'plugin'            => 'redactor'   // 'markdown'
            ],
        ],

        // View files path
        'views' => [
            'index'         => '//web/_base/index',
            'create'        => '//web/_base/create',    
            'update'        => '//web/_base/update',
            '_grid_column'  => '//web/_base/_grid_column',
            '_form'         => '//web/_base/_form',
            '_form_seo'     => '//web/_base/_form_seo',
            '_tree'         => '//web/_base/_tree',
        ],

        // Texts
        'texts' => [
            'entity_label'      => 'Static page',
            'entities_label'    => 'Static pages',

            'index_title'       => 'Manage pages',
            'add_button'        => 'Add page',
            'create_title'      => 'Create page',
            'panel_title'       => 'Sort pages',
            'list_title'        => 'Pages list',

            // Success messages
            'created_success'   => 'New page created successfully',
            'updated_success'   => 'Page updated successfully',

            // Disable
            'disable_success'   => 'Page DISABLED successfully',
            'disable_error'     => 'Page could not be DISABLED',
            'disable_confirm'   => '<h3>Are you sure you want to <span class=\'text-danger\'>DISABLE</span> this page?</h3><p><u class=\'text-danger\'>WARNING:</u> This page will not be visible.</p>',

            // Enable
            'enable_success'    => 'Page ENABLED successfully',
            'enable_error'      => 'Page could not be ENABLED',
            'enable_confirm'    => '<h3>Are you sure you want to <span class=\'text-success\'>ENABLE</span> this page?</h3><p>The page is going to be visible.</p>',

            // Delete
            'delete_success'    => 'Page DELETED successfully',
            'delete_error'      => 'Page could not be DELETED',
            'delete_confirm'    => '<h3><i class="icon wb-alert-circle text-danger"></i> Are you sure you want to <span class=\'text-danger\'>DELETE</span> this page?</h3><p><strong>WARNING:</strong> The page will be permanently removed. Consider disabling it.</p>',

            // Other
            'empty_text'        => 'No pages found',
        ]
    ],
];

<?php
/**
 * I18N configuration
 */
return [
    // Source from i18n configuration is loaded: 'file' (default) or 'db'
    'source_type' => 'file',

    // Default Language
    'default_language' => 'es',

    // Extra supported languages
    'extra_languages' => [], // ['en','de'],    // <--- It defines a MULTI-LANGUAGE application

    // Translations
    'translations' => [
        // Translations categories
        'categories' => [
            'app'       => 'App (global)',
            'frontend'  => 'Frontend',
            'backend'   => 'Backend'
        ],

        // Default category for translations management pages
        'default_category'  => 'app'
    ],
];

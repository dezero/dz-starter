<?php
/**
 * Site URL rules
 */
return[
    'gii'       => 'gii',

    // Front page
    ''                              => 'user/login',    // 'frontend/home'
    'home'                          => 'frontend/home',
    'backend'                       => 'user/login',

    // Custom URLs              
    'products'                      => 'frontend/home/products',
    'product/<id:\d+>'              => 'frontend/product',
    'page/<id:\d+>'                 => 'frontend/page',
    'category'                      => 'frontend/category',
    'category/<id:\d+>'             => 'frontend/category',

    // Account URLs
    'account/register'              => 'frontend/account/register',
    'account/login'                 => 'frontend/account/login',
    'account/professional'          => 'frontend/account/professional',
    'account/logout'                => 'frontend/account/logout',
    'account/professional/request'  => 'frontend/account/requestProfessional',
    'account/profile'               => 'frontend/account/profile',

    // Cart & checkout URLs
    'cart'                          => 'frontend/cart',
    'checkout'                      => 'frontend/checkout',
    'checkout/beforePayment'        => 'frontend/checkout/beforePayment',
    'checkout/finish'               => 'frontend/checkout/finish',
    'checkout/redsys'               => 'frontend/redsys',
    'checkout/login'                => 'frontend/account/login',
    'checkout/register'             => 'frontend/account/register',

    // Other URL's
    'about'                         => 'frontend/about',
    'contact'                       => 'frontend/contact',
    'search'                        => 'frontend/product/search',
    'error/location'                => 'frontend/error/location',

    // API path rules
    // 'api/v1/<controller:\w+>' => 'api/<controller>',
    // 'api/v1/<controller:\w+>/<id:\w+>' => 'api/<controller>/item',
    // 'api/v1/<controller:\w+>/<id:\w+>/<action:\w+>' => 'api/<controller>/<action>',
    // 'api/v1/<controller:\w+>/<id:\w+>/<action:\w+>/<entity_id:\w+>' => 'api/<controller>/<action>',
    // 'api/v1/<controller:\w+>/<action:\w+>' => 'api/<controller>',

    // '<action:\w+>' => 'frontend/<action>',
    '<controller:\w+>/<id:\d+>'     => '<controller>/view',
    '<controller:\w+>/<action:\w+>' => '<controller>/<action>',
    // '<controller:\w+>/<action:\w+>/<id:\d+>' => '<controller>/<action>',

    // Finally, slug (SEO URL)
    // '<slug:[a-zA-Z0-9-]+>'          => 'frontend/seo',
];

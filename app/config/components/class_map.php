<?php
/**
 * Mapper for classes names
 * 
 * Load it via Yii::app()->config->get('components.class_map')
 */
return [
    // Custom USER models
    'dz\modules\user\models\User'           => 'user\models\User',

    // Custom WEB models
    'dz\modules\web\models\WebContact'      => 'web\models\WebContact',

    // Custom COMMERCE models
    // 'dzlab\commerce\models\LineItem'  => 'commerce\models\LineItem',
    // 'dzlab\commerce\models\Order'     => 'commerce\models\Order',
    // 'dzlab\commerce\models\Product'   => 'commerce\models\Product',
    // 'dzlab\commerce\models\Variant'   => 'commerce\models\Variant',
];

<?php
/**
 * Product types configuration
 * 
 * Load it via Yii::app()->config->get('components.products')
 */
return [
    // <PRODUCT_TYPE> "product" is the default product type
    'product' => [
        'name'                  => Yii::t('app', 'My product'),

        // Product information tab
        'main_category_type'    => 'commerce_family',
        'is_multilanguage'      => false,
        'is_description'        => true,
        'is_seo_fields'         => true,

        // Images & videos tab
        'is_image'              => true,
        'is_video'              => false,
        'is_link'               => false,

        // Related prodcuts tab
        'is_related_products'   => true,

        // Actions allowed
        'is_disable_allowed'    => true,
        'is_delete_allowed'     => false,

        // View files path
        'views' => [
            'index'         => '//commerce/product/_base/index',
            'create'        => '//commerce/product/_base/create',    
            'update'        => '//commerce/product/_base/update',
            '_form'         => '//commerce/product/_base/_form',
            '_form_seo'     => '//commerce/product/_base/_form_seo',
            '_grid_column'  => '//commerce/product/_base/_grid_column',
            '_header_menu'  => '//commerce/product/_base/_header_menu',
            '_header_title' => '//commerce/product/_base/_header_title',
            '_sidebar'      => '//commerce/product/_base/_sidebar',
            '_tree'         => '//commerce/product/_base/_tree',
        ],

        // Texts
        'texts' => [
            'entity_label'      => 'Product',
            'entities_label'    => 'Products',

            'index_title'       => 'Manage products',
            'add_button'        => 'Add product',
            'create_title'      => 'Create product',

            // Success messages
            'created_success'   => 'New product created successfully',
            'updated_success'   => 'Product updated successfully',

            // Disable
            'disable_success'   => 'Product DISABLED successfully',
            'disable_error'     => 'Product could not be DISABLED',
            'disable_confirm'   => '<h3>Are you sure you want to <span class=\'text-danger\'>DISABLE</span> this product?</h3><p><u class=\'text-danger\'>WARNING:</u> Products will not be available for purchase.</p>',

            // Enable
            'enable_success'    => 'Product ENABLED successfully',
            'enable_error'      => 'Product could not be ENABLED',
            'enable_confirm'    => '<h3>Are you sure you want to <span class=\'text-success\'>ENABLE</span> this product?</h3><p>Products will be available for purchase.</p>',

            // Other
            'empty_text'        => 'No products found',
            'delete_confirm'    => '<h3><i class="icon wb-alert-circle text-danger"></i> Are you sure you want to <span class=\'text-danger\'>DELETE</span> this product?</h3><p><strong>WARNING:</strong> All the product data will be permanently removed. Consider disabling it.</p>',
        ]
    ],
];

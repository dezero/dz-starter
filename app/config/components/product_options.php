<?php
/**
 * Product options types configuration
 * 
 * Load it via Yii::app()->config->get('components.product_options')
 */
return [
    // Size
    'size' => [
        'name' => Yii::t('app', 'Size'),
        'is_editable' => true,
        'is_sortable' => true,
        'is_price' => false,
        'is_multilanguage' => false,
        'is_disable_allowed' => true,
        'is_delete_allowed' => true,

        // View files path
        'views' => [
            'index'         => '//commerce/option/_base/index',
            'create'        => '//commerce/option/_base/create',    
            'update'        => '//commerce/option/_base/update',
            '_grid_column'  => '//commerce/option/_base/_grid_column',
            '_form'         => '//commerce/option/_base/_form',
            '_tree'         => '//commerce/option/_base/_tree',
        ],

        // Texts
        'texts' => [
            'entity_label'      => 'Size',
            'entities_label'    => 'Sizes',

            'index_title'       => 'Manage sizes',
            'add_button'        => 'Add size',
            'create_title'      => 'Create size',
            'list_title'        => 'Sizes list',

            // Success messages
            'created_success'   => 'New size created successfully',
            'updated_success'   => 'Size updated successfully',

            // Disable
            'disable_success'   => 'Size DISABLED successfully',
            'disable_error'     => 'Size could not be DISABLED',
            'disable_confirm'   => '<h3>Are you sure you want to <span class=\'text-danger\'>DISABLE</span> this size?</h3><p><u class=\'text-danger\'>WARNING:</u> Sizes will not be available for products.</p>',

            // Enable
            'enable_success'    => 'Size ENABLED successfully',
            'enable_error'      => 'Size could not be ENABLED',
            'enable_confirm'    => '<h3>Are you sure you want to <span class=\'text-success\'>ENABLE</span> this size?</h3><p>Sizes will be available for products.</p>',

            // Other
            'empty_text'        => 'No sizes found',
            'delete_confirm'    => '<h3><i class="icon wb-alert-circle text-danger"></i> Are you sure you want to <span class=\'text-danger\'>DELETE</span> this size?</h3><p><strong>WARNING:</strong> All the product variants with this size will be permanently removed. Consider disabling it.</p>',
        ]
    ],

    // Extras
    'extra' => [
        'name' => Yii::t('app', 'Extras'),
        'is_editable' => true,
        'is_sortable' => true,
        'is_price' => true,
        'is_multilanguage' => true,
        'is_disable_allowed' => true,
        'is_delete_allowed' => true,

        // View files path
        'views' => [
            'index'         => '//commerce/option/extra/index',
            'create'        => '//commerce/option/extra/create',    
            'update'        => '//commerce/option/extra/update',
            '_grid_column'  => '//commerce/option/extra/_grid_column',
            '_form'         => '//commerce/option/extra/_form',
            '_tree'         => '//commerce/option/extra/_tree',
            '_tree_main'    => '//commerce/option/extra/_tree_main',
        ],

        // Texts
        'texts' => [
            // Category
            'entity_label'          => 'Extra',
            'entities_label'        => 'Extras',
            'index_title'           => 'Manage extras',
            'add_button'            => 'Add extra',
            'create_title'          => 'Create extra',
            'list_title'            => 'Extras list',
            'empty_text'            => 'No extras found',

            // Category - Success messages
            'created_success'       => 'New extra created successfully',
            'updated_success'       => 'Extra updated successfully',

            // Category - Disable
            'disable_success'       => 'Extra DISABLED successfully',
            'disable_error'         => 'Extra could not be DISABLED',
            'disable_confirm'       => '<h3>Are you sure you want to <span class=\'text-danger\'>DISABLE</span> this extra?</h3><p><u class=\'text-danger\'>WARNING:</u> Extras will not be available for bikes.</p>',

            // Category - Enable
            'enable_success'        => 'Extra ENABLED successfully',
            'enable_error'          => 'Extra could not be ENABLED',
            'enable_confirm'        => '<h3>Are you sure you want to <span class=\'text-success\'>ENABLE</span> this extra?</h3><p>Extras will be available for bikes.</p>',

            // Category - Delete
            'delete_success'        => 'Extra DELETED successfully',
            'delete_error'          => 'Extra could not be DELETED',
            'delete_confirm'        => '<h3><i class="icon wb-alert-circle text-danger"></i> Are you sure you want to <span class=\'text-danger\'>DELETE</span> this extra?</h3><p><strong>WARNING:</strong> Please, consider disabling it.</p>',

            // Options
            'options_label'             => 'Options',
            'option_empty_text'         => 'No options found',
            'option_add_button'         => 'Add option',
            'option_create_title'       => 'Create option',
            'option_empty_text'         => 'No extras found',

            // Options - Success messages
            'option_created_success'    => 'New option created successfully',
            'option_updated_success'    => 'Option updated successfully',

            // Options - Disable
            'option_disable_success'    => 'Option DISABLED successfully',
            'option_disable_error'      => 'Option could not be DISABLED',
            'option_disable_confirm'    => '<h3>Are you sure you want to <span class=\'text-danger\'>DISABLE</span> this option?</h3><p><u class=\'text-danger\'>WARNING:</u> This extra option will not be available for bikes.</p>',

            // Options - Enable
            'option_enable_success'     => 'Option ENABLED successfully',
            'option_enable_error'       => 'Option could not be ENABLED',
            'option_enable_confirm'     => '<h3>Are you sure you want to <span class=\'text-success\'>ENABLE</span> this option?</h3><p>Extra option will be available for bikes.</p>',

            // Options - Delete
            'option_delete_success'     => 'Option DELETED successfully',
            'option_delete_error'       => 'Option could not be DELETED',
            'option_delete_confirm'     => '<h3><i class="icon wb-alert-circle text-danger"></i> Are you sure you want to <span class=\'text-danger\'>DELETE</span> this option?</h3><p><strong>WARNING:</strong> Please, consider disabling it.</p>',
        ]
    ],
];
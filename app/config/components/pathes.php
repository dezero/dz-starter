<?php
/**
 * Directory pathes
 * 
 * Load it via Yii::app()->config->get('components.pathes')
 */
return[
    // Webroot relative path
    'webRootPath' => 'www',

    // Default relative path where files are stored
    'filesPath' => 'www/files',

    // Default relative path where temporary files are stored
    'tempPath' => 'www/files/tmp',

    // Default relative path where images are stored
    'imagesPath' => 'www/files/images',

    // PRIVATE path
    'privatePath' => 'storage',

    // PRIVATE temp path
    'privateTempPath' => 'storage/tmp',

    // IMPORT Excel path
    'importExcelPath' => 'storage/files/import'
];
<?php
/**
 * Preset images configuration
 * 
 * Load it via Yii::app()->config->get('components.images')
 */
return [
    // Default UNIX file permissions
    // --> IMPORTANT: It must be in NUMERIC format starting WITHOUT 0
    'default_permissions' => 755,
    
	// Presets or thumbnails
	'presets' => [
		'original'	=> [
			'name' => 'Tamaño original',
		],
		'large' => [
			'name' 			=> 'Tamaño BASE - 700x700 (prefijo "B_")',
			'prefix' 		=> 'B_',
			'width'			=> 700,
			'height'		=> 700,
			'is_upscale'	=> false
		],
		'medium'=> [
			'name' 			=> 'Tamaño SMALL IMAGE - 300x300 (prefijo "S_")',
			'prefix'		=> 'S_',
			'width'			=> 300,
			'height'		=> 300,
			'is_upscale'	=> false
		],
		'small' => [
			'name' 			=> 'Tamaño THUMBNAIL - 140x140 (prefijo "T_")',
			'prefix'		=> 'T_',
			'width'			=> 140,
			'height'		=> 140,
			'is_upscale'	=> false
		],
	],
];
<?php
/**
 * Web content block configuration
 * 
 * Load it via Yii::app()->config->get('components.web_block')
 */
return [
    // Home SEO block
    'home_seo' => [
        'block_title'       => 'SEO block',
        'is_multilanguage'  => true,
        'attributes'        => [
            'title' => [
                'label'             => Yii::t('backend', 'Title'),
                'is_translatable'   => true,
                'default_value'     => 'Welcome to my page'
            ],
            'body' => [
                'label'             => Yii::t('backend', 'Content'),
                'is_translatable'   => true,
                'min_height'        => 200,
                'default_value'     => 'Default content text for this field'
            ],
        ]
    ],

    // About page block
    'about_page' => [
        'block_title'       => 'About page',
        'is_multilanguage'  => true,
        'attributes'        => [
            'title' => [
                'is_translatable' => true
            ],
            'body' => [
                'label'             => 'Content',
                'is_translatable'   => true,
                'min_height'        => 100
            ],
        ]
    ],

    // Contact page block
    'contact_page' => [
        'block_title'       => 'Contact form',
        'is_multilanguage'  => true,
        'attributes'        => [
            'title' => [
                'is_translatable' => true
            ],
            'body' => [
                'label'             => 'Header text',
                'is_translatable'   => true,
                'min_height'        => 100
            ],
            'link_title' => [
                'is_translatable'   => true,
                'label'             => 'Image description',
            ],
        ]
    ],

    // Footer left block
    'footer_left' => [
        'block_title'       => 'Footer - Left column',
        'is_multilanguage'  => true,
        'attributes'        => [
            'body' => [
                'is_translatable'   => true,
                'min_height'        => 100
            ]
        ]
    ],

    // Footer center column
    'footer_center' => [
        'block_title'       => 'Footer - Center column',
        'is_multilanguage'  => true,
        'attributes'        => [
            'body' => [
                'is_translatable'   => true,
                'min_height'        => 100
            ]
        ]
    ],

    // Footer right column
    'footer_right' => [
        'block_title'       => 'Footer - Right column',
        'is_multilanguage'  => true,
        'attributes'        => [
            'body' => [
                'is_translatable'   => true,
                'min_height'        => 100
            ]
        ]
    ],

    // Order finished page block
    'order_finished' => [
        'block_title'       => 'Order finished page',
        'is_multilanguage'  => true,
        'attributes'        => [
            'title' => [
                'is_translatable' => true
            ],
            'body' => [
                'label'             => 'Content',
                'is_translatable'   => true,
                'min_height'        => 100
            ],
        ]
    ],
];

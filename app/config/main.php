<?php
/**
 * main.php
 *
 * This file holds the configuration settings of your application
 **/

// Params local file (not included on GIT repository)
$vec_params = require DZ_CONFIG_PATH . DIRECTORY_SEPARATOR . 'local'. DIRECTORY_SEPARATOR .'params.php';

return [
	'name' => getenv('SITE_NAME'),

    // Default theme
	'theme' => 'backend',
	
	// Custom paths
	'basePath' => dirname(__FILE__) . DIRECTORY_SEPARATOR . '..',
    'runtimePath' => DZ_BASE_PATH . DIRECTORY_SEPARATOR . 'storage'. DIRECTORY_SEPARATOR .'runtime',
    'viewPath' => Yii::getAlias('@core') . DIRECTORY_SEPARATOR . 'views',

	// Preloading components
	'preload' => ['log'],

	// Aliases
	'aliases' => require DZ_CONFIG_PATH . DIRECTORY_SEPARATOR .'common'. DIRECTORY_SEPARATOR . 'aliases.php',

	// autoloading model and component classes
	'import' => [
        // Glogal DzLog class
        '@core.src.helpers.DzLog',

		// Yii-user module --> http://www.yiiframework.com/extension/yii-user/
		'@modules.user.models.*',
		'@modules.user.components.*',
		
		// Remeber filters --> http://www.yiiframework.com/extension/remember-filters-gridview/
		'@lib.ERememberFiltersBehavior',
		
		// Clear filters --> http://www.yiiframework.com/extension/clear-filters-gridview
		'@lib.EButtonColumnWithClearFilters',
							
		// AuditTrail module --> http://www.yiiframework.com/extension/audittrail/
		// '@modules.auditTrail.models.AuditTrail',

		// Bootstrap
		'@bootstrap.behaviors.*',
		'@bootstrap.helpers.*',
		'@bootstrap.widgets.*',
	],

    // application components
    'components' => require DZ_CONFIG_PATH . DIRECTORY_SEPARATOR .'common'. DIRECTORY_SEPARATOR . 'components.php',

    // Yii modules
    'modules' => require DZ_CONFIG_PATH . DIRECTORY_SEPARATOR .'common'. DIRECTORY_SEPARATOR . 'modules.php',

    // Direct controller mapping
    'controllerMap' => [
        'help'  => '\dz\controllers\HelpController'
    ],

    // Locale settings
    'language' => 'es',         // <--- COPY THIS VALUE TO "default_language" property on /app/config/components/i18n.php
    'sourceLanguage' => '00',
    'timeZone' => 'Europe/Madrid',

	// application-level parameters that can be accessed
	// using Yii::app()->params['paramName']
	'params' => $vec_params,
];

<?php
/**
 * prod.php TEMPLATE configuration file
 *
 * This file holds the configuration settings of PRODUCTION environments
 **/
return array(
	'components' => array(
	),
	'params' => array(
		'yii.debug' => false,
		'yii.traceLevel' => 0,
		'yii.handleErrors'   => APP_CONFIG_NAME !== 'test',
	)
);
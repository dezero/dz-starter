<?php
/**
 * env.php TEMPLATE configuration file
 *
 * This file holds the configuration settings of DEVELOPMENT environment
 **/
return array(
	'preload' => array('kint'),
	
	'modules' => array(
		// Gii tool
		'gii' => array(
			'class'	=>'system.gii.GiiModule',
			'password'=>'gii',
			// If removed, Gii defaults to localhost only. Edit carefully to taste.
			'ipFilters' => array('127.0.0.1','::1','192.168.1.111'),
			'generatorPaths' => array(
				'@core.src.gii'          // Dz-Gii generators
			)
		),
	),

	// Autoloading models and components classes 
	'import' => array(
		// Gii related components
		'@lib.gii.giix-components.*',
	),

	// Components
    /*
	'components' => array(
		// Logs
		'log' => array(
			'class'=>'CLogRouter',
			'routes' => array(
				array(
		            'class' => '@lib.yii-db-profiler.DbProfileLogRoute',
		            'countLimit' => 1, 			// How many times the same query should be executed to be considered inefficient
	                'slowQueryMin' => 0.01, 	// Minimum time for the query to be slow
		        ),
			)
		)
	),
    */

	// Global params array
	'params' => array(
		'environment' => 'dev',
		'yii.handleErrors' => true,
		'yii.debug' => true,
		'yii.traceLevel' => 3,
	)
);
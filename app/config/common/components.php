<?php
/**
 * Components configuration
 * 
 * ATTENTION!! THIS FILE MUST BE INCLUDED FROM "main.php" AND "console.php" configuration files
 */

// Params local file (not included on GIT repository)
$vec_params = require DZ_CONFIG_PATH . DIRECTORY_SEPARATOR . 'local'. DIRECTORY_SEPARATOR .'params.php';

return [
    //------------------------------------------------------------------------------------
    //   Y I I    C O M P O N E N T S
    //------------------------------------------------------------------------------------

    // Authorization manager
    'authManager' => [
        'class' => '\dz\modules\auth\components\DbAuthManager',     // 'CDbAuthManager',
        'connectionID' => 'db',
        'itemTable' => 'user_auth_item',
        'itemChildTable' => 'user_auth_item_child',
        'assignmentTable' => 'user_auth_assignment',
        'behaviors' => [
            [
                'class' => '\dz\modules\auth\components\AuthBehavior',
                'admins' => ['Admin', 'superadmin', 'admin'],
            ]
        ],
    ],

    // Client Script --> http://www.yiiframework.com/extension/nlsclientscript/
    'clientScript' => [
        // 'class'          => '@lib.NLSClientScript',
        'class'             => '\dz\web\ClientScript', // '@core.components.DzClientScript',
        'excludePattern'    => '/\.tpl/i', //js regexp, files with matching paths won't be filtered is set to other than 'null'
        //'includePattern'  => '/\.php/' //js regexp, only files with matching paths will be filtered if set to other than 'null'
        
        'mergeJs' => false, //!YII_DEBUG, //def:true
        'compressMergedJs' => false, //def:false

        'mergeCss' => FALSE, //!YII_DEBUG, //def:true
        'compressMergedCss' => false, //def:false    

        // 'serverBaseUrl' => 'http://localhost', //can be optionally set here
        'mergeAbove' => 1, //def:1, only "more than this value" files will be merged,
        'curlTimeOut' => 5, //def:5, see curl_setopt() doc
        'curlConnectionTimeOut' => 10, //def:10, see curl_setopt() doc

        'appVersion'=> require DZ_CONFIG_PATH . DIRECTORY_SEPARATOR . '_assets_version.php' , //if set, it will be appended to the urls of the merged scripts/css

        'scriptMap' => [
            'jquery.js' => $vec_params['baseUrl'] .'/themes/backend/js/jquery.min.js',
            'jquery.min.js' => $vec_params['baseUrl'] .'/themes/backend/js/jquery.min.js',
            // 'jquery-ui.min.js' => $vec_params['baseUrl']. '/themes/backend/js/jquery-ui-1.9.2.custom.min.js',
        ],

        'coreScriptPosition' => CClientScript::POS_END,
        'defaultScriptFilePosition' => CClientScript::POS_END,
    ],

    // Translate core messages
    'coreMessages' => [
        'basePath' => DZ_APP_PATH . DIRECTORY_SEPARATOR .'core'. DIRECTORY_SEPARATOR .'messages'
    ],

    // Error layout
    'errorHandler' => [
        'errorAction' => 'frontend/error', // 'site/error'
    ],

    // Log
    'log' => [
        'class' => 'CLogRouter',
        'routes' => require DZ_CONFIG_PATH . DIRECTORY_SEPARATOR . 'components' . DIRECTORY_SEPARATOR . 'logs.php',
    ],
    
    // Message translation using database and/or PHP files
    'messages' => [
        'class' => '\dz\i18n\MessageSource',

        // Use message translation from database (comment this for disable database translation)
        'dbMessages' => [
            'sourceMessageTable' => 'translation_source',
            'translatedMessageTable' => 'translation_message'
        ],

        // Use message translation from PHP files (comment this for disable PHP translation)
        'phpMessages' => [
            'basePath' => DZ_APP_PATH . DIRECTORY_SEPARATOR .'core'. DIRECTORY_SEPARATOR .'messages'
        ]
    ],

    'request' => [
        'class'   => '\dz\web\HttpRequest'  // '@core.components.web.DzHttpRequest'
    ],

    // Session manager (increase logout timeout)
    // @see http://www.yiiframework.com/forum/index.php/topic/13733-standard-yii-logout-timeout/page__st__20
    /*
    'session' => [
        'class' => 'CDbHttpSession',
        'timeout' => 86400 // Just one day
    ],
    */

    // Sessions stored in database
    'session' => [
        'class' => '\dz\web\DbHttpSession',
        'connectionID' => 'db',
        'sessionTableName' => 'user_session',
        'autoStart' => TRUE,    // Default value
        'autoCreateSessionTable' => FALSE,
        'timeout' => 86400 // Just one day
    ],

    // uncomment the following to enable URLs in path-format
    'urlManager' => [
        // 'class'          => '\dz\web\UrlManager',
        'class'             => '\frontend\components\UrlManager',
        'urlFormat'         => 'path',
        'rules'             => require DZ_CONFIG_PATH . DIRECTORY_SEPARATOR .'components'. DIRECTORY_SEPARATOR .'routes.php',
        'appendParams'      => FALSE,                // Important! If it's TRUE, CGridView sorting and pager won't work
        'showScriptName'    => FALSE,
        'urlSuffix'         => '/',
    ],

    // WebUser component
    'user' => [
        'class' => '\user\components\WebUser',

        // enable cookie-based authentication
        'allowAutoLogin' => true,

        // Value returned for AJAX callbacks when the session times out
        'loginRequiredAjaxResponse' => 'DZ_LOGIN_REQUIRED',

        // encrypting method (php hash function)
        'hash_method' => 'md5',

        // Allow login to DISABLED users
        'allow_login_disabled' => false,

        // User register URL
        'register_url' => ['/user/register'],

        // Login page URL
        'login_url' => ['/user/login'],

        // page after login
        'returnUrl' => ['/user/admin'],

        // page after logout
        'returnLogoutUrl' => ['/user/login'],

        // Reset password URL
        'reset_password_url' => ['/user/password/reset'],

        // Verify email URL
        'verify_email_url' => ['/user/email/verify'],
        
        // timeout in seconds after which user is logged out if inactive.
        // If this property is not set, the user will be logged out after the current session expires
        // -> 43200 = 12 hours
        'authTimeout' => 86400,
        'absoluteAuthTimeout' => 86400,

        // Users with full access
        // 'admins' => ['admin', 'superadmin'],
    ],
    
    // View Renderer
    'viewRenderer'=>[
        'class'         => '\dz\base\ViewRenderer',    // '@core.components.DzViewRenderer',
        'fileExtension' => '.tpl.php',
    ],
    
    // Defaults to Widgets
    'widgetFactory' => [
        'widgets' => [
            'ERedactorWidget' => [
                'options' => [
                    'lang' => 'en',
                    'minHeight' => 100,
                    'buttonSource' => TRUE,
                    'buttons' => [
                        // 'formatting', '|',
                        'bold', 'italic', 'underline', 'unorderedlist', 'orderedlist', 'html'
                        // 'unorderedlist', 'orderedlist', 'outdent', 'indent',
                        // '|', 'scriptbuttons', 'specialcharacters', 'spellchecker'
                        //, '|', 'image', 'link', // 'file', 
                    ],
                    'formattingTags' => ['p', 'blockquote', 'h1', 'h2', 'h3']
                ],
                /*
                'plugins' => [
                    'scriptbuttons' => [
                        'js' => ['scriptbuttons.js']
                    ],
                    'specialcharacters' => [
                        'js' => ['specialcharacters.js']
                    ],
                    'spellchecker' => [
                        'css' => ['jquery.spellchecker.css'],
                        // 'js' => ['jquery.spellchecker.min.js', 'spellchecker.js'],
                        'js' => ['jquery.spellchecker.js', 'spellchecker.js'],
                    ]
                ],
                */
            ],
        ],
    ],
    

    //------------------------------------------------------------------------------------
    //   C O N T R I B    C O M P O N E N T S
    //------------------------------------------------------------------------------------

    // Bootstrap component
    'bootstrap' => [
        'class' => '@bootstrap.components.BsApi',
    ],

    // dateFormatter
    'dateFormatter' => [
        'class' => 'CDateFormatter',
        'params' => ['es'],
    ],

    /*
    // Dezero - dompdf library (custom component)
    'dompdf' => [
        // 'class'  => '@core.components.DzDompdf.DzDompdf',
        'class' => '\dz\pdf\DzDompdf'
        'libraryPath' => '@lib.dompdf',
        'destinationPath' => '@www.files.pdf',
        'constants' => [
            'DOMPDF_ENABLE_PHP' => TRUE,
            'DOMPDF_ENABLE_REMOTE' => TRUE,
            'DOMPDF_DEFAULT_PAPER_SIZE' => 'a4'
        ]           
    ],
    */

    // Image
    'image' => [
        'class' => '@lib.image.CImageComponent',
        // GD or ImageMagick
        'driver' => 'GD',

        // ImageMagick setup path
        // 'params' => ['directory'=>'D:/Program Files/ImageMagick-6.4.8-Q16'],
    ],

    // IWI Image
    'iwi' => [
        'class' => '@lib.iwi.IwiComponent',
        // GD or ImageMagick
        'driver' => 'GD',
    ],

    // MobileDetect
    /*
    'mobileDetect' => [
        'class' => '@lib.MobileDetect.MobileDetect'
    ],
    */

    /*
    // WKHtmlToPDF library
    'pdf' => array(
        'class' => '\dz\pdf\Wkhtmltopdf'
    ),
    */


    //------------------------------------------------------------------------------------
    //   D Z    C O R E    C O M P O N E N T S
    //------------------------------------------------------------------------------------

    // ClassMap component
    'classMap' => [
        'class' => '\dz\components\ClassMap',
    ],

    // Configuration component
    'config' => [
        'class' => '\dz\components\Config',
    ],

    // File manipulation
    'file' => [
        'class' => '\dz\components\File',
    ],

    // I18N
    'i18n' => [
        'class' => '\dz\i18n\I18N',
    ],

    // Javascript variables
    'javascript' => [
        'class' => '\dz\web\Javascript', // '@core.components.DzJavascript',
    ],

    // L10N
    'l10n' => [
        'class' => '\dz\i18n\L10N',
    ],

    // Number format
    'number' => [
        'class' => '\dz\components\Number',
        'numberFormat' => ['decimals' => 2, 'decimalSeparator' => ',', 'thousandSeparator' => '.'],
    ],

    // Paths
    'path' => [
        'class' => '\dz\components\Path',
        'pathes' => require DZ_CONFIG_PATH . DIRECTORY_SEPARATOR . 'components' . DIRECTORY_SEPARATOR .'pathes.php',
    ],

    // ZIP compression
    'zip' => [
        'class' => '\dz\components\Zip',
    ],


    //------------------------------------------------------------------------------------
    //   D Z    M O D U L E    C O M P O N E N T S
    //------------------------------------------------------------------------------------
    
    // Backend component class
    'backendManager' => [
        'class' => '\admin\components\BackendManager'
    ],

    // Backups component classes
    'backupManager' => [
        'class' => '\dz\modules\admin\components\BackupManager'
    ],

    // Web block component class
    'blockManager' => [
        'class' => '\dz\modules\web\components\BlockManager',
    ],

    // Category component classes
    'categoryManager' => [
        'class' => '\dz\modules\category\components\CategoryManager',
        // 'class' => '\category\components\CategoryManager'
    ],

    // Web content component class
    'contentManager' => [
        'class' => '\dz\modules\web\components\ContentManager',
    ],

    // Comment component classes
    /*
    'commentManager' => [
        // 'class' => '\dz\modules\comment\components\CommentManager',
        'class' => '\comment\components\CommentManager',
    ],
    'commentList' => [
        'class' => '\dz\modules\comment\components\CommentList',
        // 'class' => '\comment\components\CommentList'
    ],
    */

    // FileManager component class
    'fileManager' => [
        'class' => '\dz\modules\asset\components\FileManager'
    ],

    /*
    // Google Analytics component class
    'googleAnalytics' => [
        // 'class' => '\dz\modules\web\components\GoogleAnalyticsManager'
        'class' => '\web\components\GoogleAnalyticsManager',
    ],
    */

    // Mail component class
    'mail' => [
        'class' => '\dz\modules\settings\components\MailManager',

        // Available models with tokens for mail.
        // You must define tokens on mailTokens() and mailHelp() methods
        'available_model_tokens' => [
            '\user\models\User',
            [
                'class' => '\web\models\WebContact',
                'name'  => 'CONTACT MESSAGES'
            ],
        ]
    ],

    // Mapbox component
    'mapbox' => [
        'class' => '\dz\modules\search\components\MapboxManager'
    ],

    // Metatags component class
    'metatags' => [
        'class' => '\dz\modules\web\components\MetatagsManager',

        // SEO description
        // 'description' => ''
    ],

    /*
    // Notification component classes
    'notificationManager' => [
        'class' => '\dz\modules\notification\components\NotificationManager',
        // 'class' => '\notification\components\NotificationManager'
    ],
    */

    // SEO component class
    'seo' => [
        'class' => '\dz\modules\web\components\SeoManager'
    ],

    // Settings component class
    'settings' => [
        'class' => '\dz\modules\settings\components\SettingsManager'
    ],

    // Sitemap component class
    /*
    'sitemap' => [
        // 'class' => '\dz\modules\web\components\SitemapManager'
        'class' => '\web\components\SitemapManager'
    ],
    */

    // Users component classes
    'userManager' => [
        'class' => '\dz\modules\user\components\UserManager',

        // Register login access in log files
        'registerLoginAccess' => false
    ],

    
    //------------------------------------------------------------------------------------
    //   D Z    C O N T R I B    C O M P O N E N T S
    //------------------------------------------------------------------------------------

    // Commerce component classes
    'cartManager' => [
        'class' => '\dz\modules\commerce\components\CartManager',
        // 'class' => '\commerce\components\CartManager'
    ],
    'checkoutManager' => [
        'class' => '\dz\modules\commerce\components\CheckoutManager',
        // 'class' => '\commerce\components\CheckoutManager',

        // Gateways
        /*
        'gateways' => [
            'dummy' => [
                'class' => '\dz\modules\commerce\gateways\DummyGateway',
            ],
            'redsys' => [
                'class' => '\dz\modules\commerce\gateways\RedsysGateway',
            ]
        ]
        */
    ],
    'customerManager' => [
        'class' => '\dz\modules\commerce\components\CustomerManager'
        // 'class' => '\commerce\components\CustomerManager'
    ],
    'discountManager' => [
        'class' => '\dz\modules\commerce\components\DiscountManager'
        // 'class' => '\commerce\components\DiscountManager'
    ],
    'orderManager' => [
        'class' => '\dz\modules\commerce\components\OrderManager'
        // 'class' => '\commerce\components\OrderManager'
    ],
    'productManager' => [
        'class' => '\dz\modules\commerce\components\ProductManager'
        // 'class' => '\commerce\components\ProductManager'
    ],
    'productOptionManager' => [
        'class' => '\dz\modules\commerce\components\ProductOptionManager'
    ],

    // Redsys payment component class
    /*
    'redsys' => [
        'class' => '\dz\modules\commerce\components\RedsysComponent'
    ],
    */


    //------------------------------------------
    //   C U S T O M    C O M P O N E N T S
    //------------------------------------------

    // Frontend component class
    'frontendManager' => [
        'class' => '\dz\modules\web\components\FrontendManager'
        // 'class' => '\frontend\components\FrontendManager'
    ],
];

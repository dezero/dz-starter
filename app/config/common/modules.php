<?php
/**
 * Module configuration
 * 
 * ATTENTION!! THIS FILE MUST BE INCLUDED FROM "main.php" AND "console.php" configuration files
 */
return [

    //------------------------------------------------------------------------------------
    //   C O R E    D Z    M O D U L E S
    //------------------------------------------------------------------------------------

    // Administration module : logs, backups, app info
    'admin' => [
        // 'class' => '\dz\modules\admin\Module',
        'class' => '\admin\Module',
    ],

    // API module
    /*
    'api' => [
        // 'class' => '\dz\modules\api\Module',
        'class' => '\api\Module'
    ],
    */

    // Asset files module
    'asset' => [
        // 'class' => '\dz\modules\asset\Module',
        'class' => '\asset\Module',
    ],

    // AuditTrail module --> http://www.yiiframework.com/extension/audittrail/
    /*
    'auditTrail' => [
        'userClass' => 'User', // the class name for the user object
        'userIdColumn' => 'id', // the column name of the primary key for the user
        'userNameColumn' => 'username', // the column name of the primary key for the user
    ],
    */
    
    // Auth module --> http://www.yiiframework.com/extension/auth/
    'auth' => [
        // 'class' => '\dz\modules\auth\Module',
        'class' => '\auth\Module',

        // when enabled authorization items cannot be assigned children of the same type.
        'strictMode' => TRUE,

        // the name of the user model class
        'userClass' => 'User',

        // the name of the user id column
        'userIdColumn' => 'id',

        // the name of the user name column
        'userNameColumn' => 'username',
    
        // the layout used by the module
        'appLayout' => '@theme.views.layouts.html',

        // the path to view files to use with this module.
        'viewDir' => NULL,
    ],


    // Category module: cateogories and tags
    'category' => [
        // 'class' => '\dz\modules\category\Module',
        'class' => '\category\Module'
    ],

    // Comments module
    /*
    'comment' => [
        'class' => '\dz\modules\comment\Module',
        // 'class' => '\comment\Module'
    ],
    */
    /*
    // Notifications module
    'notification' => [
        'class' => '\dz\modules\notification\Module',
        // 'class' => '\notification\Module'
    ],
    */
    // Search module
    'search' => [
        // 'class' => '\dz\modules\search\Module',
        'class' => '\search\Module',
    ],
    
    // Settings module: country, currency, language and option settings
    'settings' => [
        // 'class' => '\dz\modules\settings\Module',
        'class' => '\settings\Module'
    ],

    // Yii-user module --> http://www.yiiframework.com/extension/yii-user/
    'user' => [
        // 'class' => '\dz\modules\user\Module',
        'class' => '\user\Module',
    ],

    // Web module: SEO, web contact and web content
    'web' => [
        // 'class' => '\dz\modules\web\Module',
        'class' => '\web\Module'
    ],


    //------------------------------------------------------------------------------------
    //   C O N T R I B    D Z    M O D U L E S
    //------------------------------------------------------------------------------------
    
    // Commerce module
    /*
    'commerce' => [
        'class' => '\dzlab\commerce\Module',
        // 'class' => '\commerce\Module',
    ],
    */


    // Wordpress module
    /*
    'wordpress' => [
        'class' => '\dzlab\wordpress\Module',
        // 'class' => '\wordpress\Module',
    ],
    */


    //------------------------------------------------------------------------------------
    //   C U S T O M    M O D U L E S
    //------------------------------------------------------------------------------------

    'frontend' => [
        'class' => '\frontend\Module'
    ],
];
<?php
/**
* Get server name
*
* File created automatically from Yii application
*
*/
$server_name = 'project.local';
if ( isset($_SERVER['SERVER_NAME']) AND $_SERVER['SERVER_NAME'] != $server_name )
{
	switch ( $_SERVER['SERVER_NAME'] )
	{
        case 'localhost':
        case 'project.local':
            $server_name = $_SERVER['SERVER_NAME'];
        break;
	}
}
return $server_name;

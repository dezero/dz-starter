<?php
/**
 * Parameters shared by all applications.
 *
 */

// Get dynamic server name
$server_name_file = DZ_CONFIG_PATH . DIRECTORY_SEPARATOR . 'server-name.php';
$server_name = file_exists($server_name_file) ? require($server_name_file): 'localhost';

return [
	'adminEmail' => getenv('SITE_EMAIL'),
    'basePath' => getenv('BASE_PATH'),      // '/Users/fabian/www/project.local',
    'baseUrl' => getenv('SITE_URL'),        // 'http://'. $server_name .'/project.local',
	'temporaryPath' => false,

	// Session timeou (just one day)
	'session_timeout' => 86400, 

	// Variables yo EXPORT int Javascript as globals
    'js_globals' => [
        'base_url' => getenv('SITE_URL'), // 'http://'. $server_name .'/project.local',
    ],

    // Databse BACKUP Command
    'backup_command' => '/usr/local/bin/mysqldump',
    'restore_command' => '/usr/local/bin/mysql',
    'zip_command' => '/usr/bin/zip',
    'git_command' => '/usr/local/bin/git',

	// Mail custom params
    'mail' => [
        'is_enabled' => true,
        'is_test' => true,
        'test_email' => 'test@dezero.es',
        
        'contact' => 'test@dezero.es',
        'cron_notification' => ['fabian.ruiz@gmail.com', 'test@dezero.es']
    ],
];
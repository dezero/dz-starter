<?php
/**
 * console-local.php TEMPLATE file
 *
 * This file should have the LOCAL configuration settings that will be merged to the console.php
 *
 * This configurations should be only related to your development machine
 */

return [
    'name' => getenv('SITE_NAME'),
    
    'components' => [
        // Database connection
        'db' => [
            'class' => '\dz\db\DbConnection',
            'pdoClass' => '\dz\db\DzPDO',
            'connectionString' => getenv('DB_DRIVER') .':host='. getenv('DB_SERVER') .';port='. getenv('DB_PORT') .';dbname=' . getenv('DB_DATABASE'),
            'emulatePrepare' => TRUE,
            'username' => getenv('DB_USER'),     // 'root',
            'password' => getenv('DB_PASSWORD'), // 'root00'
            'charset' => 'utf8',

            'enableProfiling' => TRUE,          // Log SQL statements being executed
            'enableParamLogging' => FALSE,      // Log the values that are bound to a prepare SQL statement
        ],
        'request' => [
            'hostInfo' => getenv('SITE_URL'),
            'baseUrl' => '/',
            'scriptUrl' => '',
        ],
    ]
];

<?php
/**
 * main-local.php TEMPLATE file
 *
 * This file should have the LOCAL configuration settings that will be merged to the main.php
 *
 * This configurations should be only related to your development machine
 */

// Get dynamic server name
$server_name_file = dirname(__FILE__) . DIRECTORY_SEPARATOR . 'server-name.php';
$server_name = file_exists($server_name_file) ? require($server_name_file): 'localhost';

return [
	'name' => getenv('SITE_NAME'),
	
	'components' => [
		// Database connection
		'db'=>[
            'class' => '\dz\db\DbConnection',
			'pdoClass' => '\dz\db\DzPDO',
            'connectionString' => getenv('DB_DRIVER') .':host='. getenv('DB_SERVER') .';port='. getenv('DB_PORT') .';dbname=' . getenv('DB_DATABASE'),
			'emulatePrepare' => true,
			'username' => getenv('DB_USER'),
            'password' => getenv('DB_PASSWORD'),
			'charset' => 'utf8',

			'enableProfiling' => true,			// Log SQL statements being executed
			'enableParamLogging' => true,		// Log the values that are bound to a prepare SQL statement
		],
		
		// Base URL
		'request' => [
			'baseUrl' => getenv('SITE_URL'),     // 'http://'. $server_name .'/<project.local>'
		],

        // Logs
        /*
        'log' => [
            'class'=>'CLogRouter',
            'routes' => [
                [
                    'class' => '@lib.yii-db-profiler.DbProfileLogRoute',
                    'countLimit' => 1,          // How many times the same query should be executed to be considered inefficient
                    'slowQueryMin' => 0.01,     // Minimum time for the query to be slow
                ],
            ]
        ]
        */
	]
];

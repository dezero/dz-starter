<?php
/*
|--------------------------------------------------------------------------
| Message translation to DEUTSCH
|--------------------------------------------------------------------------
|
| NOTE, this file must be saved in UTF-8 encoding.
*/

return [
    // Months
    'January' => 'Januar',
    'February' => 'Februar',
    'March' => 'März',
    'April' => 'April',
    'May' => 'Mai',
    'June' => 'Juni',
    'July' => 'Juli',
    'August' => 'August',
    'September' => 'September',
    'October' => 'Oktober',
    'November' => 'November',
    'December' => 'Deze1mber',
];
<?php
/*
|--------------------------------------------------------------------------
| Traducción de mensajes CORE de Zii
|--------------------------------------------------------------------------
|
| NOTE, this file must be saved in UTF-8 encoding.
*/
return [
	// CGridView
	'Displaying {start}-{end} of 1 result.|Displaying {start}-{end} of {count} results.' => 'Página {page} de {pages}<br/>Mostrando {start}-{end} de 1 resultado|Página {page} de {pages}<br/>Mostrando {start}-{end} de {count} resultados',
	'Update' => 'Editar',
];
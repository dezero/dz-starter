<?php
/**
 * This is the configuration for generating message translations.
 * It is used by the 'yiic message' command.
 *
 * ./yiic message app/messages/config.php
 */

return [
    'sourcePath'  => DZ_BASE_PATH,
    'messagePath' => DZ_BASE_PATH . DIRECTORY_SEPARATOR .'www'. DIRECTORY_SEPARATOR .'files'. DIRECTORY_SEPARATOR .'messages',
    'filesTypes' => ['php'],
    'languages' => ['es', 'ca'],
    'overwrite' => true,
    'removeOld' => true,
    'sort' => true,
    'translator' => 'Yii::t',
    'exclude' => [
        '.phpintel',
        '.git',
        '.gitignore',
        '/app/core',
        '/app/vendor',
        '/node_modules',

        // Exclude backend theme
        // '/www/themes/backend',
    ],
];

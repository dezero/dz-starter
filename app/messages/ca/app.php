<?php
/*
|--------------------------------------------------------------------------
| Message translation to CATALÀ
|--------------------------------------------------------------------------
|
| NOTE, this file must be saved in UTF-8 encoding.
*/

return [
    // Months
    'January' => 'Gener',
    'February' => 'Febrer',
    'March' => 'Març',
    'April' => 'Abril',
    'May' => 'Maig',
    'June' => 'Juny',
    'July' => 'Juliol',
    'August' => 'Agost',
    'September' => 'Setembre',
    'October' => 'Octubre',
    'November' => 'Novembre',
    'December' => 'Desembre',
];
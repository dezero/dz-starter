<?php
/**
  * Module for managing Yii's built-in authorization manager (AuthManager).
 */

namespace auth;

class Module extends \dz\modules\auth\Module
{
    /**
     * Load specific CSS or JS files for this module
     */
    // public $cssFiles = ['auth.css'];
    // public $jsFiles = ['auth.js'];


    /**
     * This method is called when the module is being created
     * you may place code here to customize the module or the application
     */
    public function init()
    {
        parent::init();
    }
}

<?php
/**
 * Module to manage users
 */

namespace user;

class Module extends \dz\modules\user\Module
{
    /**
     * Load specific CSS or JS files for this module
     */
    // public $cssFiles = ['user.css'];
    // public $jsFiles = ['user.js'];


    /**
     * This method is called when the module is being created
     * you may place code here to customize the module or the application
     */
    public function init()
    {
        parent::init();
    }
}

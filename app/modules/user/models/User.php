<?php
/**
 * @package user\models 
 */

namespace user\models;

use dz\db\DbCriteria;
use dz\helpers\DateHelper;
use dz\helpers\StringHelper;
use dz\modules\user\components\UserIdentity;
use dz\modules\user\models\User as CoreUser;
use Yii;

/**
 * User model class for "user_users" database table
 *
 * Columns in table "user_users" available as properties of the model,
 * followed by relations of table "user_users" available as properties of the model.
 *
 * -------------------------------------------------------------------------
 * COLUMN FIELDS
 * -------------------------------------------------------------------------
 * @property integer $id
 * @property string $username
 * @property string $password
 * @property string $email
 * @property string $firstname
 * @property string $lastname
 * @property string $verification_code
 * @property integer $last_login_date
 * @property string $last_login_ip
 * @property string $status_type
 * @property integer $is_verified_email
 * @property integer $last_verification_date
 * @property integer $is_force_change_password
 * @property integer $last_change_password_date
 * @property string $default_role
 * @property integer $is_superadmin
 * @property integer $disable_date
 * @property integer $disable_uid
 * @property integer $created_date
 * @property integer $created_uid
 * @property integer $updated_date
 * @property integer $updated_uid
 * @property string $uuid
 *
 * -------------------------------------------------------------------------
 * RELATIONS
 * -------------------------------------------------------------------------
 * @property mixed $sessions
 * @property mixed $createdUser
 * @property mixed $users
 * @property mixed $disableUser
 * @property mixed $users1
 * @property mixed $updatedUser
 * @property mixed $users2
 */
class User extends CoreUser
{
	/**
	 * Constructor
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
	
	
	/**
	 * Returns the validation rules for attributes
	 */
	public function rules()
	{            
		return [
			['username, password, email, created_date, created_uid, updated_date, updated_uid', 'required'],
			['last_login_date, is_verified_email, last_verification_date, is_force_change_password, last_change_password_date, is_superadmin, disable_date, disable_uid, created_date, created_uid, updated_date, updated_uid', 'numerical', 'integerOnly' => true],
			['username', 'length', 'min' => 5, 'max'=>100, 'message' => Yii::t('user', "Incorrect username (length between 5 and 50 characters).")],
            ['username', 'unique', 'message' => Yii::t('user', "This username has already been taken"), 'on' => 'insert'],
            // ['username', 'match', 'pattern' => '/^[A-Za-z0-9_]+$/u', 'message' => Yii::t('user', "Username must contain only letters and numbers.")],
            ['password', 'length', 'max' => 128, 'min' => 5, 'message' => Yii::t('user', "Incorrect password (minimal length 5 symbols).")],
            ['firstname, lastname', 'length', 'max'=> 100],
			['email, verification_code, last_login_ip', 'length', 'max'=> 255],
            ['email', 'email'],
            ['email', 'unique', 'message' => Yii::t('user', "This email address has already been taken"), 'on' => 'insert'],
            ['default_role', 'length', 'max'=> 64],
			['uuid', 'length', 'max' => 36],
			['status_type', 'in', 'range' => [self::STATUS_ACTIVE, self::STATUS_DISABLED, self::STATUS_BANNED, self::STATUS_PENDING]],
            ['loginas_token', 'safe'],
			['firstname, lastname, verification_code, last_login_date, last_login_ip, status_type, is_verified_email, last_verification_date, is_force_change_password, last_change_password_date, is_superadmin, default_role, disable_date, disable_uid, uuid', 'default', 'setOnEmpty' => true, 'value' => null],
			['id, username, password, email, firstname, lastname, verification_code, last_login_date, last_login_ip, status_type, is_verified_email, last_verification_date, is_force_change_password, last_change_password_date, is_superadmin, default_role, disable_date, disable_uid, created_date, created_uid, updated_date, updated_uid, uuid, role_filter', 'safe', 'on' => 'search'],

            // Custom validation rules
            ['password', 'validate_login', 'on' => 'login'],
            ['verify_password', 'validate_verify_password', 'on' => 'insert, change_password'],
            ['email', 'validate_verify_email', 'on' => 'password_recovery' ]
		];
	}
	

	/**
	 * Define relations with other objects
	 *
	 * There are four types of relations that may exist between two active record objects:
	 *   - BELONGS_TO: e.g. a member belongs to a team;
	 *   - HAS_ONE: e.g. a member has at most one profile;
	 *   - HAS_MANY: e.g. a team has many members;
	 *   - MANY_MANY: e.g. a member has many skills and a skill belongs to a member.
	 */
	public function relations()
	{
        $vec_relations = parent::relations();

        // Customr relations
        // $vec_relations['customer'] = [self::BELONGS_TO, Customer::class, ['id' => 'user_id']];
     
        return $vec_relations;
	}


    /**
     * Returns the attribute labels
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'username' => Yii::t('app', 'Username'),
            'password' => Yii::t('app', 'Password'),
            'email' => Yii::t('app', 'Email'),
            'firstname' => Yii::t('app', 'First name'),
            'lastname' => Yii::t('app', 'Last name'),
            'verification_code' => Yii::t('app', 'Verification Code'),
            'last_login_date' => Yii::t('app', 'Last Login'),
            'last_login_ip' => Yii::t('app', 'Last Login Ip'),
            'status_type' => Yii::t('app', 'Status Type'),
            'is_verified_email' => Yii::t('app', 'Verified Email?'),
            'last_verification_date' => Yii::t('app', 'Last Email Verification Date'),
            'is_force_change_password' => Yii::t('app', 'Force Password Change?'),
            'last_change_password_date' => Yii::t('app', 'Last Change Password'),
            'default_role' => Yii::t('app', 'Default Role'),
            'is_superadmin' => Yii::t('app', 'Is Superadmin'),
            'disable_date' => Yii::t('app', 'Disable Date'),
            'disable_uid' => null,
            'created_date' => Yii::t('app', 'User Since'),
            'created_uid' => null,
            'updated_date' => Yii::t('app', 'Updated Date'),
            'updated_uid' => null,
            'uuid' => Yii::t('app', 'Uuid'),
            'userSessions' => null,
            'createdUser' => null,
            'userUsers' => null,
            'disableUser' => null,
            'userUsers1' => null,
            'updatedUser' => null,
            'userUsers2' => null,

            // Custom labels
            'verify_password' => Yii::t('app', 'Repeat Password'),
        ];
    }


    /**
     * Generate an ActiveDataProvider for search form of this model
     *
     * Used in CGridView
     */
    public function search()
    {
        $criteria = new DbCriteria;
        
        $criteria->with = [];
        // $criteria->together = true;

        $criteria->compare('t.id', $this->id);
        $criteria->compare('t.username', $this->username, true);
        $criteria->compare('t.email', $this->email, true);

        // Find by firstname, lastname or firstname together with lastname
        if ( $this->firstname )
        {
            $criteria->addCondition('t.firstname LIKE CONCAT("%", :name, "%") OR t.lastname LIKE CONCAT("%", :name, "%") OR CONCAT(t.firstname, " ", t.lastname) LIKE CONCAT("%", :name, "%")');
            $criteria->addParams([':name' => $this->firstname]);
        }

        // Exclude "Anonymous" for non-superadmin user
        if ( Yii::app()->user->id > 1 )
        {
            $criteria->addCondition('t.id > 0');

            // Exclude SUPERADMIN?
            if ( ! Yii::app()->user->checkAccess('user_full_manage') )
            {
                $criteria->compare('t.is_superadmin', 0);
            }
        }

        // Filter by role
        if ( $this->role_filter )
        {
            $criteria->join = "INNER JOIN user_auth_assignment u ON u.userid = t.id";
            $criteria->addCondition('u.itemname = "'. $this->role_filter.'"');
        }

        return new \CActiveDataProvider($this, [
            'criteria' => $criteria,
            'pagination' => ['pageSize' => 30],
            'sort' => ['defaultOrder' => ['id' => true]]
        ]);
    }
}

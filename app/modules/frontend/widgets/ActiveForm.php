<?php
/**
 * Custom ActiveForm widget class
 */

namespace frontend\widgets;

use BsArray;
// use BsActiveForm;
use frontend\helpers\Html;
use Yii;

// Import core CGridView
Yii::import('@bootstrap.widgets.BsActiveForm');

class ActiveForm extends \BsActiveForm
{
    /**
     * Displays a summary of validation errors for one or several models.
     * @param mixed $models the models whose input errors are to be displayed.
     * @param string $header a piece of HTML code that appears in front of the errors
     * @param string $footer a piece of HTML code that appears at the end of the errors
     * @param array $htmlOptions additional HTML attributes to be rendered in the container div tag.
     * @return string the error summary. Empty if no errors are found.
     */
    public function errorSummary($models, $header = null, $footer = null, $htmlOptions = [])
    {
        if ( ! $this->enableAjaxValidation && !$this->enableClientValidation )
        {
            return Html::errorSummary($models, $header, $footer, $htmlOptions);
        }
        BsArray::defaultValue('id', $this->id . '_es_', $htmlOptions);
        
        $html = Html::errorSummary($models, $header, $footer, $htmlOptions);
        if ($html === '')
        {
            if ($header === null)
            {
                $header = '<p>' . Yii::t('yii', 'Please fix the following input errors:') . '</p>';
            }
            Html::addCssClass(BsHtml::$errorSummaryCss, $htmlOptions);
            Html::addCssStyle('display:none', $htmlOptions);
            $html = Html::tag('div', $htmlOptions, $header . '<ul><li>dummy</li></ul>' . $footer);
        }

        $this->summaryID = $htmlOptions['id'];

        foreach ( is_array($models) ? $models : [$models] as $model )
        {
            foreach ( $model->getSafeAttributeNames() as $attribute )
            {
                $this->_summaryAttributes[] = Html::activeId($model, $attribute);
            }
        }

        return $html;
    }


    /**
     * Displays the first validation error for a model attribute.
     * @param CModel $model the data model
     * @param string $attribute the attribute name
     * @param array $htmlOptions additional HTML attributes to be rendered in the container div tag.
     * @param boolean $enableAjaxValidation whether to enable AJAX validation for the specified attribute.
     * @param boolean $enableClientValidation whether to enable client-side validation for the specified attribute.
     * @return string the validation result (error display or success message).
     */
    public function error($model, $attribute, $htmlOptions = [], $enableAjaxValidation = true, $enableClientValidation = true)
    {
        if ( ! $this->enableAjaxValidation)
        {
            $enableAjaxValidation = false;
        }
        
        if ( ! $this->enableClientValidation)
        {
            $enableClientValidation = false;
        }
        
        if ( ! $enableAjaxValidation && !$enableClientValidation)
        {
            return Html::error($model, $attribute, $htmlOptions);
        }
        
        $id = Html::activeId($model, $attribute);
        $inputID = BsArray::getValue('inputID', $htmlOptions, $id);
        unset($htmlOptions['inputID']);
        BsArray::defaultValue('id', $inputID . '_em_', $htmlOptions);
        $option = [
            'id' => $id,
            'inputID' => $inputID,
            'errorID' => $htmlOptions['id'],
            'model' => get_class($model),
            'name' => $attribute,
            'enableAjaxValidation' => $enableAjaxValidation,
            'inputContainer' => 'div.form-group', // Bootstrap requires this,
            'errorCssClass' => $this->errorMessageCssClass,
            'successCssClass' => $this->successMessageCssClass
        ];

        $optionNames = [
            'validationDelay',
            'validateOnChange',
            'validateOnType',
            'hideErrorMessage',
            'inputContainer',
            'errorCssClass',
            'successCssClass',
            'validatingCssClass',
            'beforeValidateAttribute',
            'afterValidateAttribute',
        ];
        
        foreach ( $optionNames as $name )
        {
            if ( isset($htmlOptions[$name]) )
            {
                $option[$name] = BsArray::popValue($name, $htmlOptions);
            }
        }

        if ( $model instanceof \CActiveRecord && !$model->isNewRecord )
        {
            $option['status'] = 1;
        }

        if ( $enableClientValidation )
        {
            $validators = BsArray::getValue('clientValidation', $htmlOptions, []);
            $attributeName = $attribute;
           
            if ( ($pos = strrpos($attribute, ']') ) !== false && $pos !== strlen($attribute) - 1) // e.g. [a]name
            {
                $attributeName = substr($attribute, $pos + 1);
            }

            foreach ( $model->getValidators($attributeName) as $validator )
            {
                if ( $validator->enableClientValidation )
                {
                    if ( ($js = $validator->clientValidateAttribute($model, $attributeName) ) != '' )
                    {
                        $validators[] = $js;
                    }
                }
            }

            if ($validators !== [])
            {
                $option['clientValidation'] = "js:function(value, messages, attribute) {\n" . implode(
                        "\n",
                        $validators
                    ) . "\n}";
            }
        }

        $html = Html::error($model, $attribute, $htmlOptions);

        if ($html === '')
        {
            $htmlOptions['type'] = $this->helpType;
            BsHtml::addCssStyle('display:none', $htmlOptions);
            $html = Html::help('', $htmlOptions);
        }

        $this->attributes[$inputID] = $option;
        return $html;
    }
}
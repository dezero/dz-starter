<?php
/**
 * UrlManager component class file
 *
 * @author Fabián Ruiz <fabian@dezero.es>
 * @link http://dezero.es/
 * @copyright Copyright &copy; 2020 Dezero
 */

namespace frontend\components;

use dz\helpers\Url;
use dz\modules\web\models\Seo;
use dz\web\UrlManager as CoreUrlManager;
use Yii;

/**
 * Custom UrlManager component
 */
class UrlManager extends CoreUrlManager
{
    /**
     * Query parameter name that contains a language
     */
    public $languageParam = 'language';


    /**
     * Initializes the application component.
     */
    public function init()
    {
        // First of all, load all rules
        parent::init();

        // Get defined routes
        $vec_routes = Yii::app()->config->get('components.routes');

        // Current URL path info ("product/2", "user/login" or "product-slug-url")
        $path_info =  Url::path_info();

        // Frontend URL's via slug SEO
        if ( !empty($path_info) && !isset($vec_routes[$path_info]) && ( Yii::app()->user->id == 0 || Yii::app()->user->isCustomer() ) )
        {
            // Create temporary & specific rule for this URL slug
            // For example "my-product-url" => 'frontend/product/slug'
            $seo_model = Yii::app()->seo->get_current_seo();
            if ( $seo_model )
            {
                switch ( $seo_model->entity_type )
                {
                    case 'product':
                    case 'category':
                        $this->addRules([
                            $path_info  => 'frontend/'. $seo_model->entity_type .'/slug',
                        ]);
                    break;
                }
            }
        }
    }
}
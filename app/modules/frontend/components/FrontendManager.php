<?php
/**
 * Frontend Manager
 * 
 * Helper classes collection for frontend theme
 */

namespace frontend\components;

use dz\db\DbCriteria;
use dz\helpers\Log;
use dz\helpers\StringHelper;
use dz\modules\web\components\FrontendManager as CoreFrontendManager;
use Yii;

class FrontendManager extends CoreFrontendManager
{

}

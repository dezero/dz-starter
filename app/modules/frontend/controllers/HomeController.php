<?php
/*
|--------------------------------------------------------------------------
| Controller class for frontend pages - Home page
|--------------------------------------------------------------------------
*/

namespace frontend\controllers;

use dz\helpers\Html;
use dz\helpers\Url;
use frontend\controllers\_base\BaseFrontController as Controller;
use Yii;

class HomeController extends Controller
{
    /**
     * Home page
     */
    public function actionIndex()
    {
        $this->render('//home/index', [
            // 'block_seo_model'       => Yii::app()->blockManager->load_block('home_seo')
        ]);
    }
}
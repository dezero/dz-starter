<?php
/*
|--------------------------------------------------------------------------
| Controller class for frontend pages - Category pages
|--------------------------------------------------------------------------
*/

namespace frontend\controllers;

use dz\helpers\Html;
use dz\modules\category\models\Category;
use dz\modules\commerce\models\Product;
use frontend\controllers\_base\BaseFrontController as Controller;
use Yii;

class CategoryController extends Controller
{
    /**
     * Category VIEW page
     * 
     * It will redirect from /category/{id} -> /<seo-url>
     */
    public function actionIndex($id = '')
    {
        // Current category model
        if ( !empty($id) )
        {
            $category_model = Category::findOne($id);
            if ( ! $category_model )
            {
                throw new \CHttpException(404, Yii::t('app', 'The requested page does not exist.'));
            }

            // There's an assigned SEO URL for this category? If yes, redirect to semantic SEO URL via 301
            if ( $category_model->seo )
            {
                $this->redirect($category_model->url(), true, 301);
            }

            return $this->view($id);
        }

        // Get all products
        else
        {
            $this->render('//category/index', [
                'category_model'        => null,
                // 'vec_category_list'     => Yii::app()->productManager->category_list(),
                // 'vec_product_models'    => Yii::app()->productManager->product_list(),
            ]);
        }
    }


    /**
     * Slug action
     * 
     * Called form \frontend\components\UrlManager::init()
     */
    public function actionSlug()
    {
        // Find SEO model for current language
        $seo_model = Yii::app()->seo->get_current_seo();
        if ( $seo_model )
        {
            return $this->view($seo_model->entity_id);
        }
        
        throw new \CHttpException(404, Yii::t('app', 'The requested page does not exist.'));
    }


    /**
     * Real VIEW action
     */
    public function view($id)
    {
        // Load current category
        $category_model = Category::findOne($id);
        if ( ! $category_model )
        {
            throw new \CHttpException(404, Yii::t('app', 'The requested page does not exist.'));
        }

        $this->render('//category/index', [
            'category_model'            => $category_model,
            // 'vec_category_list'     => Yii::app()->productManager->category_list(),
            // 'vec_product_models'    => Yii::app()->productManager->product_list(),
        ]);
    }
}
<?php
/*
|--------------------------------------------------------------------------
| Controller class for contact page
|--------------------------------------------------------------------------
*/

namespace frontend\controllers;

use dz\helpers\Html;
use frontend\controllers\_base\BaseFrontController as Controller;
use user\models\User;
use web\models\WebContact;
use Yii;

class ContactController extends Controller
{
    /**
     * Contact form page
     */
    public function actionIndex()
    {
        // WebContact model
        $contact_model = Yii::createObject(WebContact::class);

        // Insert new model?
        if ( isset($_POST['WebContact']) )
        {
            if ( empty($_POST['WebContact']['city']) )
            {
                unset($_POST['WebContact']['city']);
                $contact_model->setAttributes($_POST['WebContact']);

                // #2 - Validate contact form
                $is_validated = $contact_model->validate();

                // #3 - Validate if legal is checked
                if ( !isset($_POST['LegalAccepted']) )
                {
                    $is_validated = false;
                    $contact_model->addError('is_legal_accepted', Yii::t('form', 'You must accept the Privacy Policy'));
                }
                
                if ( $is_validated && $contact_model->save(false) )
                {
                    $contact_message = '<h4 class="alert-heading">'. Yii::t('frontend', 'Mensaje enviado!') .'</h4><p class="mb-0">'.  Yii::t('newhorizon', 'Nos pondremos en contacto con usted tan pronto como sea posible.') .'</p>';
                    Yii::app()->user->addFlash('success', $contact_message);
                    
                    $this->redirect(['/contact']);
                }
            }

            // SPAM detection
            else
            {
                $contact_model->addError('name', 'Bot spam detected');
                $contact_model->is_spam_blocked = true;
            }
        }

        // Get name and email from registered user
        $user_model = null;
        if ( Yii::app()->user->id > 0 )
        {
            $user_model = Yii::app()->user->model();
            if ( $user_model )
            {
                if ( empty($contact_model->name) )
                {
                    $contact_model->name = $user_model->fullname();
                }
                if ( empty($contact_model->email) )
                {
                    $contact_model->email = $user_model->email();
                }
            }
        }

        // Render contact page
        $this->render('//static/contact', [
            'contact_model'     => $contact_model,
            'user_model'        => $user_model,
            'block_model'       => Yii::app()->blockManager->load_block('contact_page')
        ]);
    }
}

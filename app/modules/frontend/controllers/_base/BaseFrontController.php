<?php
/*
|--------------------------------------------------------------------------
| BASE controller class for all FRONTEND PAGES
|--------------------------------------------------------------------------
*/

namespace frontend\controllers\_base;

use dz\web\Controller;
use Yii;

class BaseFrontController extends Controller
{
    /**
     * @inheritdoc
     */
    public function createWidget($className, $properties = [])
    {
        // Widget namespace conversion --> Transform from "dz.widgets.Upload" to "\dz\widgets\Upload"
        if ( preg_match("/^frontend\./", $className) )
        {
            $className = '\\'. str_replace('.', '\\', $className);
        }

        return parent::createWidget($className, $properties);
    }
}
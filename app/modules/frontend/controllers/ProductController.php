<?php
/*
|--------------------------------------------------------------------------
| Controller class for frontend pages - Product pages
|--------------------------------------------------------------------------
*/

namespace frontend\controllers;

use dz\helpers\Html;
use dz\modules\category\models\Category;
use dz\modules\commerce\models\Product;
use frontend\controllers\_base\BaseFrontController as Controller;
use Yii;

class CategoryController extends Controller
{
    /**
     * Product VIEW page
     * 
     * It will redirect from /product/{id} -> /<seo-url>
     */
    public function actionIndex($id)
    {
        $product_model = Product::findOne($id);
        if ( ! $product_model )
        {
            throw new \CHttpException(404, Yii::t('app', 'The requested page does not exist.'));
        }

        // There's an assigned SEO URL for this category? If yes, redirect to semantic SEO URL via 301
        if ( $product_model->seo )
        {
            $this->redirect($product_model->url(), true, 301);
        }

        return $this->view($id);
    }


    /**
     * Slug action
     * 
     * Called form \frontend\components\UrlManager::init()
     */
    public function actionSlug()
    {
        // Find SEO model for current language
        $seo_model = Yii::app()->seo->get_current_seo();
        if ( $seo_model )
        {
            return $this->view($seo_model->entity_id);
        }
        
        throw new \CHttpException(404, Yii::t('app', 'The requested page does not exist.'));
    }


    /**
     * Search results page
     */
    public function actionSearch()
    {
        // Product list
        $product_model = Yii::createObject(Product::class, 'search');
        $product_model->unsetAttributes();

        if ( isset($_GET['product']) )
        {
            $product_model->product_id = $_GET['product'];
        }

        if ( isset($_GET['type']) )
        {
            $product_model->main_category_id = $_GET['type'];
        }

        // Get all the products list
        $vec_product_models = $product_model->frontend_search();

        $this->render('//product/search', [
            'vec_product_models'    => $vec_product_models
        ]);
    }


    /**
     * Real VIEW action
     */
    public function view($id)
    {
        // Load current category
        $product_model = Category::findOne($id);
        if ( ! $product_model )
        {
            throw new \CHttpException(404, Yii::t('app', 'The requested page does not exist.'));
        }

        // Get current cart (Order model)
        $cart_order_model = Yii::app()->cartManager->get_order_model();

        $this->render('//category/index', [
            // Product
            'product_model'         => $product_model,

            // Variant
            'variant_model'         => $product_model->defaultVariant,

            // Cart (Order model)
            'cart_order_model'      => $cart_order_model,
        ]);
    }
}
<?php
/*
|--------------------------------------------------------------------------
| Controller class for site
|--------------------------------------------------------------------------
*/

namespace frontend\controllers;

use dz\helpers\Html;
use frontend\controllers\_base\BaseFrontController as Controller;
use Yii;

class ErrorController extends Controller
{
    /**
     * This is the action to handle external exceptions.
     */
    public function actionIndex()
    {
        // Force backend theme for NON-MEMBER registered users
        if ( Yii::app()->user->id > 0 )
        {
            Yii::app()->theme = 'backend';
        }

        if ( $error = Yii::app()->errorHandler->error )
        {
            // Errors on REST API?
            if ( preg_match("/^api\/v/", Yii::app()->request->getPathInfo()) )
            {
                $controller = new \dz\modules\api\controllers\ErrorsController('api');
                $controller->actionError();
            }

            else
            {
                if ( Yii::app()->request->isAjaxRequest )
                {
                    echo $error['message']; 
                }
                else
                {
                    $this->render('//layouts/error', $error);
                }
            }
        }
    }


    /**
     * Custom error 403
     */
    public function actionLocation()
    {
        throw new \CHttpException(403, 'Access denied for your IP address or area.');
        /*
        $vec_data = array(
            'code'          => 403,
            'message'       => 'Access denied for your IP address or area.',
            'is_geoblock'   => TRUE
        );
        $this->render('//frontend/error/index', $vec_data);
        */
    }
}

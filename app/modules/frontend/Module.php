<?php
/**
 * Module for frontend pages
 */

namespace frontend;

class Module extends \dz\web\Module
{
    /**
     * Force frontend theme
     */
    public $theme = 'frontend';


    /**
     * Default controller
     */
    public $defaultController = 'home';


    /**
     * Load specific CSS or JS files for this module
     */
    public $cssFiles = null;
    public $jsFiles = null;


    /**
     * This method is called when the module is being created
     * you may place code here to customize the module or the application
     */
    public function init()
    {
        parent::init();
    }
}

<?php
/**
 * Custom Html helper class file
 */

namespace frontend\helpers;

use dz\helpers\Html as CoreHtml;
use Yii;

class Html extends CoreHtml
{
    /**
     * @var string the CSS class for displaying error summaries.
     */
    public static $errorSummaryCss = 'alert alert-danger alert-dismissible fade show';


    /**
     * Displays the first validation error for a model attribute.
     * @param CModel $model the data model.
     * @param string $attribute the attribute name.
     * @param array $htmlOptions additional HTML attributes.
     * @return string the rendered error. Empty if no errors are found.
     */
    public static function error($model, $attribute, $htmlOptions = array())
    {
        parent::resolveName($model, $attribute); // turn [a][b]attr into attr
        $error = $model->getError($attribute);
        return !empty($error) ? self::help($error, $htmlOptions) : '';
    }


    /**
     * Generates a help text.
     * @param string $text the help text.
     * @param array $htmlOptions additional HTML attributes.
     * @return string the generated text.
     */
    public static function help($text, $htmlOptions = [])
    {
        self::addCssclass('invalid-feedback', $htmlOptions);
        return self::tag('div', $htmlOptions, $text);
    }


    /**
     * Displays a summary of validation errors for one or several models.
     * @param mixed $model the models whose input errors are to be displayed. This can be either
     * a single model or an array of models.
     * @param string $header a piece of HTML code that appears in front of the errors
     * @param string $footer a piece of HTML code that appears at the end of the errors
     * @param array $htmlOptions additional HTML attributes to be rendered in the container div tag.
     * A special option named 'firstError' is recognized, which when set true, will
     * make the error summary to show only the first error message of each attribute.
     * If this is not set or is false, all error messages will be displayed.
     * This option has been available since version 1.1.3.
     * @return string the error summary. Empty if no errors are found.
     * @see CModel::getErrors
     * @see errorSummaryCss
     */
    public static function errorSummary($model, $header=null, $footer=null, $htmlOptions=[])
    {
        $content = '';
        
        if ( !is_array($model) )
        {
            $model = [$model];
        }
        
        if ( isset($htmlOptions['firstError']) )
        {
            $firstError = $htmlOptions['firstError'];
            unset($htmlOptions['firstError']);
        }
        else
        {
            $firstError = false;
        }

        foreach( $model as $m )
        {
            foreach ( $m->getErrors() as $errors )
            {
                foreach ( $errors as $error )
                {
                    if ( $error!='' )
                    {
                        $content .= "<li>". $error ."</li>\n";
                    }
                    if ( $firstError )
                    {
                        break;
                    }
                }
            }
        }
        if ( $content !== '' )
        {
            if ( $header !== null )
            {
                $header = '<h4 class="alert-heading">'. $header .'</h4>';
            }

            if ( !isset($htmlOptions['class']) )
            {
                $htmlOptions['class'] = self::$errorSummaryCss;
            }
            else
            {
                $htmlOptions['class'] .= ' alert';
            }

            if ( !isset($htmlOptions['id']) )
            {
                $htmlOptions['id'] = 'form-messages';
            }

            // Add role="alert"
            $htmlOptions['role'] = 'alert';

            return self::tag('div', $htmlOptions, $header ."<ul>". $content ."</ul>". $footer);
        }
        else
        {
            return '';
        }
    }
}
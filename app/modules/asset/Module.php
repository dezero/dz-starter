<?php
/**
 * Module to manage asset files
 */

namespace asset;

class Module extends \dz\modules\asset\Module
{
    /**
     * Default controller
     */
    public $defaultController = 'asset';


    /**
     * Load specific CSS or JS files for this module
     */
    // public $cssFiles = ['asset.css'];
    // public $jsFiles = ['asset.js'];


    /**
     * This method is called when the module is being created
     * you may place code here to customize the module or the application
     */
    public function init()
    {
        parent::init();
    }
}

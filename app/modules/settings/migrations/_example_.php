<?php
/**
 * Migration class m200707_162426_custom_option_setting
 *
 * @link http://www.dezero.es/
 */

use dz\db\Migration;
use dz\helpers\DateHelper;
use dz\helpers\StringHelper;

class m200707_162426_custom_option_setting extends Migration
{
    /**
     * This method contains the logic to be executed when applying this migration.
     */
    public function up()
    {
        // New setting: my_custom_option
        // -------------------------------------------------------------------------
        $this->insert('setting', [
            'name'          => 'my_custom_option',
            'value'         => '25',
            'type'          => 'commerce',
            'title'         => 'Custom option',
            'description'   => 'Custom option description',
            'created_date'  => time(),
            'created_uid'   => 1,
            'updated_date'  => time(),
            'updated_uid'   => 1,
            'uuid'          => StringHelper::UUID()
        ]);

        return true;
    }


    /**
     * This method contains the logic to be executed when removing this migration.
     */
    public function down()
    {
        return false;
    }
}


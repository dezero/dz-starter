<?php
/*
|--------------------------------------------------------------------------
| Controller class to admin commerce settings
|--------------------------------------------------------------------------
*/

namespace settings\controllers;

use dz\modules\settings\models\Setting;
use dz\web\Controller;
use Yii;

class CommerceController extends Controller
{
    /**
     * Default action
     */
    public function actionIndex()
    {
        $vec_settings_models = [];

        // My custom option
        $vec_settings_models['my_custom_option'] = Setting::findOne('my_custom_option');

        // Insert new model?
        if ( isset($_POST['Setting']) )
        {
            if ( Yii::app()->settings->save_settings($_POST['Setting'], $vec_settings_models) )
            {
                Yii::app()->user->addFlash('success', 'Settings updated successfully');

                // Redirect to same page
                $this->redirect(['/settings/commerce']);
            }
        }
        
        // Render form page
        $this->render('update', [
            'title'                 => 'Commerce Configuration',
            'vec_settings_models'   => $vec_settings_models,
            'form_id'               => 'commerce-settings-form'
        ]);
    }
}
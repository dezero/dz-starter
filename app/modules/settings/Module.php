<?php
/**
 * Module to manage settings
 */

namespace settings;

class Module extends \dz\modules\settings\Module
{
    /**
     * Default controller
     */
    public $defaultController = 'language';


    /**
     * Load specific CSS or JS files for this module
     */
    public $cssFiles = null; // ['settings.css'];
    public $jsFiles = null; // ['settings.js'];


    /**
     * This method is called when the module is being created
     * you may place code here to customize the module or the application
     */
    public function init()
    {
        parent::init();
    }
}

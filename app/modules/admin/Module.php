<?php
/**
 * Module for administration purposes
 */

namespace admin;

class Module extends \dz\modules\admin\Module
{
    /**
     * Default controller
     */
    public $defaultController = 'log';


    /**
     * Load specific CSS or JS files for this module
     */
    public $cssFiles = null; // ['admin.css'];
    public $jsFiles = null; // ['admin.js'];


    /**
     * This method is called when the module is being created
     * you may place code here to customize the module or the application
     */
    public function init()
    {
        parent::init();
    }
}

<?php
/**
 * Backend Manager
 * 
 * Helper classes collection for backend theme
 */

namespace admin\components;

use dz\modules\admin\components\BackendManager as CoreBackendManager;
use Yii;

/**
 * BackendManager - Helper classes collection for backend theme
 */
class BackendManager extends CoreBackendManager
{
    /**
     * Init function
     */
    public function init()
    {
        parent::init();
    }


    /**
     * Get body classes
     */
    public function body_classes()
    {
        $vec_classes = parent::body_classes();

        // Exclude page-login
        if ( !in_array('page-login', $vec_classes) )
        {
            // Left sidebar column
            /*
            if ( $this->current_module == 'commerce' && ( $this->current_controller == 'product' || $this->current_controller == 'order' || $this->current_controller == 'customer') && $this->current_action == 'index' )
            {
                $vec_classes[] = 'page-aside-static';
                $vec_classes[] = 'page-aside-left';
            }
            */
        }

        return $vec_classes;
    }
}
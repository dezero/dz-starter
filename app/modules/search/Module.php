<?php
/**
 * Module to manage searches
 */

namespace search;

class Module extends \dz\modules\search\Module
{
    /**
     * Default controller
     */
    public $defaultController = 'search';


    /**
     * Load specific CSS or JS files for this module
     */
    // public $cssFiles = ['search.css'];
    // public $jsFiles = ['search.js'];


    /**
     * This method is called when the module is being created
     * you may place code here to customize the module or the application
     */
    public function init()
    {
        parent::init();
    }
}

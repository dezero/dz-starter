<?php
/**
 * Module to manage category entities
 */

namespace category;

class Module extends \dz\modules\category\Module
{
    /**
     * Default controller
     */
    public $defaultController = 'category';


    /**
     * Load specific CSS or JS files for this module
     */
    // public $cssFiles = ['category.css'];
    // public $jsFiles = ['category.js'];


    /**
     * This method is called when the module is being created
     * you may place code here to customize the module or the application
     */
    public function init()
    {
        parent::init();
    }
}

<?php
/**
 * Module to manage web content
 */

namespace web;

class Module extends \dz\modules\web\Module
{
    /**
     * Default controller
     */
    public $defaultController = 'content';


    /**
     * Load specific CSS or JS files for this module
     */
    public $cssFiles = null; // ['web.css'];
    public $jsFiles = null; // ['web.js'];


    /**
     * This method is called when the module is being created
     * you may place code here to customize the module or the application
     */
    public function init()
    {
        parent::init();
    }
}

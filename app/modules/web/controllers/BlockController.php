<?php
/*
|--------------------------------------------------------------------------
| Controller class to admin content of HOME page
|--------------------------------------------------------------------------
*/

namespace web\controllers;

use dz\modules\web\models\WebBlock;
use dz\web\Controller;
use Yii;

class BlockController extends Controller
{
    /**
     * Default action
     */
    public $defaultAction = 'home';


    /**
     * Home page
     */
    public function actionHome()
    {
        // Home SEO block
        Yii::app()->blockManager->add_block('home_seo');

        // Insert new model?
        if ( isset($_POST['WebBlock']) )
        {
            if ( Yii::app()->blockManager->save_blocks($_POST['WebBlock']) )
            {
                Yii::app()->user->addFlash('success', 'Content updated successfully');

                // Redirect to same page
                $this->redirect(['/web/block/home']);
            }
        }
        $this->render('update', [
            'title'         => 'Home page',
            'vec_models'    => Yii::app()->blockManager->get_blocks(),
            'form_id'       => 'web-block-form'
        ]);
    }


    /**
     * Contact page
     */
    public function actionContact()
    {
        // Contact header
        Yii::app()->blockManager->add_block('contact_page');

        // Insert new model?
        if ( isset($_POST['WebBlock']) )
        {
            if ( Yii::app()->blockManager->save_blocks($_POST['WebBlock']) )
            {
                Yii::app()->user->addFlash('success', 'Content updated successfully');

                // Redirect to same page
                $this->redirect(['/web/block/contact']);
            }
        }

        $this->render('update', [
            'title'         => 'Contact page',
            'vec_models'    => Yii::app()->blockManager->get_blocks(),
            'form_id'       => 'web-block-form'
        ]);
    }


    /**
     * About page
     */
    public function actionAbout()
    {
        // Contact header
        Yii::app()->blockManager->add_block('about_page');

        // Insert new model?
        if ( isset($_POST['WebBlock']) )
        {
            if ( Yii::app()->blockManager->save_blocks($_POST['WebBlock']) )
            {
                Yii::app()->user->addFlash('success', 'Content updated successfully');

                // Redirect to same page
                $this->redirect(['/web/block/about']);
            }
        }

        $this->render('update', [
            'title'         => 'About page',
            'vec_models'    => Yii::app()->blockManager->get_blocks(),
            'form_id'       => 'web-block-form'
        ]);
    }


    /**
     * Order finished page
     */
    public function actionFinished()
    {
        // Contact header
        Yii::app()->blockManager->add_block('order_finished');

        // Insert new model?
        if ( isset($_POST['WebBlock']) )
        {
            if ( Yii::app()->blockManager->save_blocks($_POST['WebBlock']) )
            {
                Yii::app()->user->addFlash('success', 'Content updated successfully');

                // Redirect to same page
                $this->redirect(['/web/block/finished']);
            }
        }

        $this->render('update', [
            'title'         => 'Order finished page',
            'vec_models'    => Yii::app()->blockManager->get_blocks(),
            'form_id'       => 'web-block-form'
        ]);
    }



    /**
     * Footer section
     */
    public function actionFooter()
    {
        // Left column
        Yii::app()->blockManager->add_block('footer_left');

        // Center column
        Yii::app()->blockManager->add_block('footer_center');

        // Right column
        Yii::app()->blockManager->add_block('footer_right');

        // Insert new model?
        if ( isset($_POST['WebBlock']) )
        {
            if ( Yii::app()->blockManager->save_blocks($_POST['WebBlock']) )
            {
                Yii::app()->user->addFlash('success', 'Content updated successfully');

                // Redirect to same page
                $this->redirect(['/web/content/footer']);
            }
        }

        $this->render('update', [
            'title'         => 'Footer blocks',
            'vec_models'    => Yii::app()->blockManager->get_blocks(),
            'form_id'       => 'web-content-form'
        ]);
    }



    /**
     * Returns an actions list of current controller related to its auth operation to check access in "DzAuthFilter"
     *
     * array('<defined_action>' => '<operation_name_to_check>')
     *
     * @return array
     */
    static public function checkAliasActions()
    {
        return [
            'home'      => 'web_manage',
            'footer'    => 'web_manage',
            'contact'   => 'web_manage',
            'about'     => 'web_manage',
            'finished'  => 'web_manage',
        ];
    }
}
#!/bin/bash

# Send files to DzLab modules directory
#
# Copy files FROM vendor/dezero directory TO DzLab modules directory

# One argument is required
if [ "$#" -ne 1 ]
then
  echo "Usage: dzlab <module_name>"
  exit 1
fi

# Get the directory of the currently executing script
DIR="$(dirname "${BASH_SOURCE[0]}")"

# Include files
INCLUDE_FILES=(
            "common/defaults.sh"
            ".env.sh"
            "common/common_env.sh"
            )
for INCLUDE_FILE in "${INCLUDE_FILES[@]}"
do
    if [[ ! -f "${DIR}/${INCLUDE_FILE}" ]] ; then
        echo "File ${DIR}/${INCLUDE_FILE} is missing, aborting."
        exit 1
    fi
    source "${DIR}/${INCLUDE_FILE}"
done

rsync -r -v -z -h --progress --exclude='.git/' ${LOCAL_LAB_PATH}$1 ${DZ_LAB_PATH}
echo "*** Copied DZ LAB module content from ${LOCAL_LAB_PATH}$1 to ${DZ_LAB_PATH}$1"

# Normal exit
exit 0


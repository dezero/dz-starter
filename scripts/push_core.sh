#!/bin/bash

# Send files to Dz Core framework
#
# Copy files FROM app/core directory TO Dz Core framework

# Get the directory of the currently executing script
DIR="$(dirname "${BASH_SOURCE[0]}")"

# Include files
INCLUDE_FILES=(
            "common/defaults.sh"
            ".env.sh"
            "common/common_env.sh"
            )
for INCLUDE_FILE in "${INCLUDE_FILES[@]}"
do
    if [[ ! -f "${DIR}/${INCLUDE_FILE}" ]] ; then
        echo "File ${DIR}/${INCLUDE_FILE} is missing, aborting."
        exit 1
    fi
    source "${DIR}/${INCLUDE_FILE}"
done

rsync -r -v -z -h --progress --exclude='.git/' ${LOCAL_CORE_PATH} ${DZ_CORE_PATH}
echo "*** Copied DZ CORE content from ${LOCAL_CORE_PATH} to ${DZ_CORE_PATH}"

# Normal exit
exit 0

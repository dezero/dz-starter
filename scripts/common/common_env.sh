#!/bin/bash

# Common Env
#
# Shared script to set various environment-related variables

# Craft paths; ; paths should always have a trailing /
LOCAL_DZ_FILES_PATH=${LOCAL_ROOT_PATH}"storage/"
REMOTE_DZ_FILES_PATH=${REMOTE_ROOT_PATH}"storage/"

# Commands to output database dumps, using gunzip -c instead of zcat for MacOS X compatibility
DB_ZCAT_CMD="gunzip -c"
DB_CAT_CMD="cat"

# For nicer user messages
PLURAL_CHAR="s"

# Sub-directories for the various backup types
DB_BACKUP_SUBDIR="db"
ASSETS_BACKUP_SUBDIR="assets"
FILES_BACKUP_SUBDIR="files"

#!/bin/bash

# Common DB
#
# Shared script to set various database-related variables
#
# Tables to exclude from the db dump
EXCLUDED_DB_TABLES=(
            "cache"
            "user_session"
            )

